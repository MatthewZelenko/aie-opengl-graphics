#version 330 core

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

out vec4 fColour;

uniform sampler2D DirtTexture;
uniform sampler2D RockTexture;
uniform sampler2D SnowTexture;
uniform sampler2D WaterTexture;

uniform vec3 CameraPos;
uniform vec3 LightPos;
uniform vec3 LightColour;
uniform float Amplitude = 1.0f;

void main()
{
	float specularStrength = 0.05f;
	vec4 diffColour;
	//grass and dirt
	float diffDot = dot(vNormal, vec3(0,1,0));
	diffColour = mix(texture(DirtTexture, vTexCoord), texture(RockTexture, vTexCoord), (1 - diffDot) + 0.5f);

	//snow
	float r1 = 0.35f;
	float r2 = 1.0f;
	float v = vPosition.y / Amplitude;
	if( v > 0.35f)
	{
		float mixValue = (r2 / r1) * (v - 0.35f);
		//diffColour = texture(WaterTexture, vTexCoord);
		diffColour = mix(diffColour, texture(SnowTexture, vTexCoord),  mixValue);
		specularStrength = mix(0.0f, 1.0f, mixValue - 0.25f);
	}

	//water
	r1 = 0.15f;
	r2 = 1.0f;
	if( v < -0.15f)
	{
		float mixValue = (r2 / r1) * (v + 0.15f);
		diffColour = texture(WaterTexture, vTexCoord);
		diffColour += mix(vec4(0, 0, 0, 1.0f), vec4(0.15f, 0.15f, 0.15f, 1.0f), mixValue);
		specularStrength = mix(0.75f, 0.0f, mixValue);
	}

	//ambient
	float ambientStrength = 0.35f;
	vec3 ambient = ambientStrength * LightColour;

	//diffuse
	vec3 norm = normalize(vNormal);
	vec3 lightDir = normalize(LightPos - vPosition);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * LightColour;

	//specular
	vec3 viewDir = normalize(CameraPos - vPosition);
	vec3 reflectDir = reflect(-lightDir, norm);
	float viewDirDot = max(0.0, dot(viewDir, reflectDir));
	float spec = pow(viewDirDot, 32);
	vec3 specular = spec * specularStrength * LightColour;

	fColour = vec4(ambient + diffuse + specular, 1.0) * diffColour;
}