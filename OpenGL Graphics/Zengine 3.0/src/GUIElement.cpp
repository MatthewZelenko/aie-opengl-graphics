#include "GUIElement.h"

GUIElement::GUIElement(Controls type)
{
	m_Type = type;
	m_Size = glm::vec2(1.0f);
}
GUIElement::~GUIElement()
{
}

void GUIElement::ProcessInput()
{

}

void GUIElement::SetPosition(glm::vec2 position)
{
	m_Position = position;
}
void GUIElement::SetSize(glm::vec2 size)
{
	m_Size = size;
}