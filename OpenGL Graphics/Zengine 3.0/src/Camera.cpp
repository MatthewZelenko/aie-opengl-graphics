#include "Camera.h"
#include <GLM\gtc\matrix_transform.hpp>

Camera::Camera()
{
}
Camera::~Camera()
{
}

void Camera::Update(float a_deltaTime)
{

}
void Camera::UpdateMatrices()
{
	m_View = glm::lookAt(m_Position, m_Position + m_Forward, m_Up);
	m_DirtyPlanes = true;
}
void Camera::UpdatePlanes()
{
	//if (m_DirtyPlanes)
	//{
		glm::mat4 transform = m_Projection * m_View;
		m_Planes[PlaneType::PLANE_RIGHT] = glm::vec4(
			transform[0][3] - transform[0][0],
			transform[1][3] - transform[1][0],
			transform[2][3] - transform[2][0],
			transform[3][3] - transform[3][0]);

		m_Planes[PlaneType::PLANE_LEFT] = glm::vec4(
			transform[0][3] + transform[0][0],
			transform[1][3] + transform[1][0],
			transform[2][3] + transform[2][0],
			transform[3][3] + transform[3][0]);

		m_Planes[PlaneType::PLANE_TOP] = glm::vec4(
			transform[0][3] - transform[0][1],
			transform[1][3] - transform[1][1],
			transform[2][3] - transform[2][1],
			transform[3][3] - transform[3][1]);

		m_Planes[PlaneType::PLANE_BOTTOM] = glm::vec4(
			transform[0][3] + transform[0][1],
			transform[1][3] + transform[1][1],
			transform[2][3] + transform[2][1],
			transform[3][3] + transform[3][1]);

		m_Planes[PlaneType::PLANE_FAR] = glm::vec4(
			transform[0][3] - transform[0][2],
			transform[1][3] - transform[1][2],
			transform[2][3] - transform[2][2],
			transform[3][3] - transform[3][2]);

		m_Planes[PlaneType::PLANE_NEAR] = glm::vec4(
			transform[0][3] + transform[0][2],
			transform[1][3] + transform[1][2],
			transform[2][3] + transform[2][2],
			transform[3][3] + transform[3][2]);

		//for (int i = 0; i < 6; ++i)
		//{
		//	m_Planes[i] = glm::normalize(m_Planes[i]);
		//}
		//m_DirtyPlanes = false;
	//}
}

void Camera::TranslateWorldForward(float a_translation)
{
	m_Position += a_translation * glm::vec3(0.0f, 0.0f, -1.0f);
	UpdateMatrices();
}
void Camera::TranslateWorldRight(float a_translation)
{
	m_Position += a_translation * glm::vec3(1.0f, 0.0f, 0.0f);
	UpdateMatrices();
}
void Camera::TranslateWorldUp(float a_translation)
{
	m_Position += a_translation * glm::vec3(0.0f, 1.0f, 0.0f);
	UpdateMatrices();
}
void Camera::TranslateForward(float a_translation)
{
	m_Position += a_translation * m_Forward;
	UpdateMatrices();
}
void Camera::TranslateRight(float a_translation)
{
	glm::vec3 right = glm::normalize(glm::cross(m_Forward, m_Up));
	m_Position += a_translation * right;
	UpdateMatrices();
}
void Camera::TranslateUp(float a_translation)
{
	m_Position += a_translation * m_Up;
	UpdateMatrices();
}
void Camera::TranslateZ(float a_translation)
{
	glm::vec3 forward = glm::normalize(glm::vec3(m_Forward.x, 0.0f, m_Forward.z));
	m_Position += a_translation * forward;
	UpdateMatrices();
}
void Camera::TranslateX(float a_translation)
{
	glm::vec3 right = glm::normalize(glm::cross(m_Forward, m_Up));
	right.y = 0.0f;
	m_Position += a_translation * right;
	UpdateMatrices();
}

void Camera::RotateAroundForward(float a_rotation)
{
	glm::quat rotation;
	rotation = glm::rotate(rotation, a_rotation, m_Forward);
	m_Up = m_Up * rotation;
	UpdateMatrices();
}
void Camera::RotateAroundRight(float a_rotation)
{
	glm::quat rotation;
	glm::vec3 right = glm::normalize(glm::cross(m_Forward, m_Up));
	rotation = glm::rotate(rotation, a_rotation, right);
	m_Up = m_Up * rotation;
	m_Forward = m_Forward * rotation;
	UpdateMatrices();
}
void Camera::RotateAroundUp(float a_rotation)
{
	glm::quat rotation;
	rotation = glm::rotate(rotation, a_rotation, m_Up);
	m_Forward = m_Forward * rotation;
	UpdateMatrices();
}
void Camera::RotateAroundWorldForward(float a_rotation)
{
	glm::quat rotation;
	rotation = glm::rotate(rotation, a_rotation, glm::vec3(0.0f, 0.0f, -1.0f));
	m_Up = m_Up * rotation;
	UpdateMatrices();
}
void Camera::RotateAroundWorldRight(float a_rotation)
{
	glm::quat rotation;
	rotation = glm::rotate(rotation, a_rotation, glm::vec3(1.0f, 0.0f, 0.0f));
	m_Up = m_Up * rotation;
	m_Forward = m_Forward * rotation;
	UpdateMatrices();
}
void Camera::RotateAroundWorldUp(float a_rotation)
{
	glm::quat rotation;
	rotation = glm::rotate(rotation, a_rotation, glm::vec3(0.0f, 1.0f, 0.0f));
	m_Forward = m_Forward * rotation;
	UpdateMatrices();
}

void Camera::SetPerspective(float a_fov, float a_aspect, float a_near, float a_far)
{
	m_Projection = glm::perspective(a_fov, a_aspect, a_near, a_far);
}
void Camera::SetView(glm::vec3& a_position, glm::vec3& a_direction, glm::vec3& a_up)
{
	m_Position = a_position;
	m_Forward = glm::normalize(a_direction);
	m_Up = glm::normalize(a_up);
	UpdateMatrices();
}
void Camera::SetUp(glm::vec3& a_up)
{
	m_Up = a_up;
	UpdateMatrices();
}
void Camera::SetForward(glm::vec3& a_forward)
{
	m_Forward = a_forward;
	UpdateMatrices();
}

void Camera::SetPosition(glm::vec3& a_position)
{
	m_Position = a_position;
	UpdateMatrices();
}
void Camera::SetXPosition(float a_position)
{
	m_Position.x = a_position;
	UpdateMatrices();
}
void Camera::SetYPosition(float a_position)
{
	m_Position.y = a_position;
	UpdateMatrices();
}
void Camera::SetZPosition(float a_position)
{
	m_Position.z = a_position;
	UpdateMatrices();
}
bool Camera::SphereInsideFrustum(BoundingSphere& a_sphere)
{
	UpdatePlanes();
	for (int i = 0; i < 6; ++i)
	{
		float d = glm::dot(glm::vec3(m_Planes[i]), a_sphere.GetCenter()) + m_Planes[i].w;

		if (d < -a_sphere.GetRadius())
		{
			return false;
		}
	}
	return true;
}
bool Camera::AABBInsideFrustum(AABB& a_aabb)
{
	UpdatePlanes();
	//for (int i = 0; i < 6; ++i)
	//{
	//	float d = glm::dot(glm::vec3(m_Planes[i]), aabb.GetMin()) + m_Planes[i].w;
	//
	//	if(d < 0)
	//
	//
	//}
	return true;
}

bool Camera::InsideFrustum(BoundObject* a_boundObject)
{
	if (a_boundObject)
	{
		BoundType type = a_boundObject->GetType();
		if (type == BOX)
		{
			AABB* aabb = (AABB*)a_boundObject;
			return AABBInsideFrustum(*aabb);
		}
		else if(type == SPHERE)
		{
			BoundingSphere* sphere = (BoundingSphere*)a_boundObject;
			return SphereInsideFrustum(*sphere);
		}
	}
	return true;
}