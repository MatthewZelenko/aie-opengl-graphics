#pragma once
#include "BoundObject.h"

class AABB : public BoundObject
{
public:
	AABB();
	~AABB();

	void Reset();
	void Fit(std::vector<FBXVertex>& a_points);

	void SetMin(glm::vec3 a_value);
	void SetMax(glm::vec3 a_value);
	void SetPosition(glm::vec3 a_position);
	void AddPosition(glm::vec3 a_position);

	glm::vec3 GetMin() { return m_Min; }
	glm::vec3 GetMax() { return m_Max; }

protected:
	glm::vec3 m_Min, m_Max;
};