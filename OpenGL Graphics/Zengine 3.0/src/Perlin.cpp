#include "Perlin.h"
#define _USE_MATH_DEFINES
#include <math.h>

float Perlin::LinearLerp(float a_a, float a_b, float a_x)
{
	return a_a * (1 - a_x) + a_b * a_x;
}
float Perlin::CosLerp(float a_a, float a_b, float a_x)
{
	float ft = a_x * M_PI;
	float f = (1 - cos(ft)) * 0.5f;
	return a_a * (1 - f) + a_b * f;
}
float Perlin::CubicLerp(float a_v0, float a_v1, float a_v2, float a_v3, float a_x)
{
	float P = (a_v3 - a_v2) - (a_v0 - a_v1);
	float Q = (a_v0 - a_v1) - P;
	float R = a_v2 - a_v0;
	float S = a_v1;
	return P * pow(a_x, 3) + Q * pow(a_x, 2) + R * a_x + S;
}

float Perlin::GenNoise1(int a_x)
{
	int x = (a_x << 13) ^ a_x;
	return (1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}
float Perlin::GenNoise2(int a_x, int a_y)
{
	int n = a_x + a_y * 57;
	n = (n << 13) ^ n;
	return (1.0f - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}

float Perlin::GenSmoothNoise1(float a_x)
{
	return GenNoise1((int)a_x) / 2 + GenNoise1((int)a_x - 1) / 4 + GenNoise1((int)a_x + 1) / 4;
}
float Perlin::GenSmoothNoise2(float a_x, float a_y)
{
	float corners = (GenNoise2((int)a_x - 1, (int)a_y - 1) + GenNoise2((int)a_x + 1, (int)a_y - 1) + GenNoise2((int)a_x - 1, (int)a_y + 1) + GenNoise2((int)a_x + 1, (int)a_y + 1)) / 8;
	float sides = (GenNoise2((int)a_x - 1, (int)a_y) + GenNoise2((int)a_x + 1, (int)a_y) + GenNoise2((int)a_x, (int)a_y - 1) + GenNoise2((int)a_x, (int)a_y + 1)) / 8;
	float center = GenNoise2((int)a_x, (int)a_y) / 5;
	return corners + sides + center;
}

float Perlin::LerpNoise1(float a_x)
{
	int ix = int(a_x);
	float fx = a_x - ix;
	float v1 = GenSmoothNoise1((float)ix);
	float v2 = GenSmoothNoise1((float)ix + 1);
	return CosLerp(v1, v2, fx);
}
float Perlin::LerpNoise2(float a_x, float a_y)
{
	int ix = int(a_x);
	float fx = a_x - ix;

	int iy = int(a_y);
	float fy = a_y - iy;

	float v1 = GenSmoothNoise2((float)ix, (float)iy);
	float v2 = GenSmoothNoise2((float)ix + 1, (float)iy);
	float v3 = GenSmoothNoise2((float)ix, (float)iy + 1);
	float v4 = GenSmoothNoise2((float)ix + 1, (float)iy + 1);

	float i1 = CosLerp(v1, v2, fx);
	float i2 = CosLerp(v3, v4, fx);
	return CosLerp(i1, i2, fy);
}

float Perlin::GenPerlinNoise1(float a_x, float a_amplitude, float a_frequency, float a_persistence)
{
	float total = 0;
	float p = a_persistence;
	int n = 8;
	for (int i = 0; i < n; ++i)
	{
		float frequency = pow(2, i) * a_frequency;
		float amplitude = pow(p, i) * a_amplitude;
		total += LerpNoise1(a_x * frequency) * amplitude;
	}
	return total;
}
float Perlin::GenPerlinNoise2(float a_x, float a_y, float a_amplitude, float a_frequency, float a_persistence)
{
	float total = 0;
	float p = a_persistence;
	int n = 8;
	for (int i = 0; i < n; ++i)
	{
		float frequency = pow(2, i) * a_frequency;
		float amplitude = pow(p, i) * a_amplitude;
		total += LerpNoise2(a_x * frequency, a_y * frequency) * amplitude;
	}
	return total;
}