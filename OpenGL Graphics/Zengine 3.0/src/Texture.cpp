#include "Texture.h"
#include "gl_core_4_4.h"
#include <STB\stb_image.h>
#include <iostream>
using namespace std;
Texture::Texture()
{
	m_ID = 0;
	m_iWrapSParam = GL_REPEAT;
	m_iWrapTParam = GL_REPEAT;
	m_iMinFilterParam = GL_LINEAR;
	m_iMagFilterParam = GL_LINEAR;
	m_iImageFormat = 0;
}
Texture::Texture(const Texture& other)
{
	m_ID = other.m_ID;
	m_strName = other.m_strName;
	m_iWidth = other.m_iWidth;
	m_iHeight = other.m_iHeight;
	m_iWrapSParam = other.m_iWrapSParam;
	m_iWrapTParam = other.m_iWrapTParam;
	m_iMinFilterParam = other.m_iMinFilterParam;
	m_iMagFilterParam = other.m_iMagFilterParam;
	m_iImageFormat = other.m_iImageFormat;
}
Texture::~Texture()
{
}
void Texture::Destroy()
{
	if (m_ID > 0)
	{
		glDeleteTextures(1, &m_ID);
		m_ID = 0;
	}
}

Texture& Texture::operator=(const Texture& other)
{
	m_ID = other.m_ID;
	m_strName = other.m_strName;
	m_iWidth = other.m_iWidth;
	m_iHeight = other.m_iHeight;
	m_iWrapSParam = other.m_iWrapSParam;
	m_iWrapTParam = other.m_iWrapTParam;
	m_iMinFilterParam = other.m_iMinFilterParam;
	m_iMagFilterParam = other.m_iMagFilterParam;
	m_iImageFormat = other.m_iImageFormat;
	return *this;
}

Texture Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, m_ID);
	return *this;
}
void Texture::UnBind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::SetWrapS(int param)
{
	if (param == GL_REPEAT ||
		param == GL_MIRRORED_REPEAT ||
		param == GL_CLAMP_TO_EDGE ||
		param == GL_CLAMP_TO_BORDER)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, param);
		m_iWrapTParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}
void Texture::SetWrapT(int param)
{
	if (param == GL_REPEAT ||
		param == GL_MIRRORED_REPEAT ||
		param == GL_CLAMP_TO_EDGE ||
		param == GL_CLAMP_TO_BORDER)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, param);
		m_iWrapSParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}
void Texture::SetMinFilter(int param)
{
	if (param == GL_NEAREST ||
		param == GL_LINEAR ||
		param == GL_NEAREST_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_LINEAR ||
		param == GL_NEAREST_MIPMAP_LINEAR)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, param);
		m_iMinFilterParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}
void Texture::SetMagFilter(int param)
{
	if (param == GL_NEAREST ||
		param == GL_LINEAR ||
		param == GL_NEAREST_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_NEAREST ||
		param == GL_LINEAR_MIPMAP_LINEAR ||
		param == GL_NEAREST_MIPMAP_LINEAR)
	{
		Bind();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, param);
		m_iMagFilterParam = param;
		UnBind();
	}
	else
	{
		cout << "Texture: Invalid param" << endl;
	}
}

Texture Texture::CreateFromFile(const std::string file)
{
	if (m_ID)
		Destroy();
	m_strName = file;
	glGenTextures(1, &m_ID);
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_iWrapSParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_iWrapTParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_iMinFilterParam);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_iMagFilterParam);

	unsigned char* image = stbi_load(file.c_str(), &m_iWidth, &m_iHeight, &m_iImageFormat, STBI_default);
	unsigned int alpha = GL_RGB;
	if (m_iImageFormat == 4)
		alpha = GL_RGBA;

	glTexImage2D(GL_TEXTURE_2D, 0, alpha, m_iWidth, m_iHeight, 0, alpha, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	UnBind();
	return *this;
}