#include "SceneObject.h"
#include <GLM\gtc\matrix_transform.hpp>


SceneObject::SceneObject():m_IsDirty(true), m_Bounds(nullptr), m_Size(1.0f)
{
}
SceneObject::~SceneObject()
{

}
void SceneObject::Destroy()
{
	delete m_Bounds;
}

void SceneObject::ProcessInput()
{

}
void SceneObject::Update()
{

}
void SceneObject::Draw(Shader a_shader)
{
	if (m_Model)
	{
		glm::mat4 mat;
		mat = glm::translate(mat, m_Position);
		mat = glm::scale(mat, m_Size);
		mat = mat * glm::mat4_cast(m_Rotation);
		a_shader.SendVec4("Colour", 1, glm::vec4(1.0f));

		m_Model->Draw(a_shader, mat);
	}
}
void SceneObject::SetModel(Model* a_model)
{
	m_Model = a_model;
}

void SceneObject::SetPosition(glm::vec3 a_position)
{
	m_Position = a_position;
	m_IsDirty = true;
}
void SceneObject::SetSize(glm::vec3 a_size)
{
	m_Size = a_size;
	m_IsDirty = true;
}
void SceneObject::SetRotation(float a_degrees, glm::vec3 a_axis)
{
	m_Rotation = glm::rotate(glm::quat(), glm::radians(a_degrees), glm::normalize(a_axis));
	m_IsDirty = true;
}
void SceneObject::SetRotation(glm::vec3 up)
{
	glm::mat4 rot;
	glm::vec3 forward = glm::normalize(glm::cross(up, glm::vec3(0, 1, 0)));
	glm::vec3 right = glm::normalize(glm::cross(up, forward));
	rot[0] = glm::vec4(right, 0);
	rot[1] = glm::vec4(up, 0);
	rot[2] = glm::vec4(forward, 0);
	m_Rotation = glm::toQuat(rot);
}
void SceneObject::AddPosition(glm::vec3 a_position)
{
	m_Position += a_position;
	m_IsDirty = true;
}
void SceneObject::AddSize(glm::vec3 a_size)
{
	m_Size += a_size;
	m_IsDirty = true;
}

void SceneObject::SetBound(BoundObject* a_bounds)
{
	m_Bounds = a_bounds;
}