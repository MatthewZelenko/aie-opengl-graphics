#version 330 core

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

uniform sampler2D Diffuse;
uniform sampler2D Specular;

uniform vec3 CameraPos;
uniform vec3 LightPos;
uniform vec3 LightColour;

uniform vec4 Colour = vec4(1.0f);

out vec4 fColour;

void main()
{
	vec4 diffColour = texture(Diffuse, vTexCoord);

	//ambient
	float ambientStrength = 0.3f;
	vec3 ambient = ambientStrength * LightColour;

	//diffuse
	vec3 norm = normalize(vNormal);
	vec3 lightDir = normalize(LightPos - vPosition);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * LightColour;

	//specular
	float specularStrength = 0.5;
	vec3 viewDir = normalize(CameraPos - vPosition);
	vec3 reflectDir = reflect(-lightDir, norm);
	float viewDirDot = max(0.0, dot(viewDir, reflectDir));
	float spec = pow(viewDirDot, 32);
	vec3 specular = spec * LightColour;

	fColour = vec4(ambient + diffuse + specular, 1.0f) * diffColour * Colour;
	fColour.a = 1.0f;
}