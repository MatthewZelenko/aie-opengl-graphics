#pragma once
#include "Application.h"
#include "Shader.h"
#include "FlyCamera.h"
#include "Texture.h"
#include "GPUTerrain.h"
#include "Terrain.h"
#include "SceneObject.h"
#include "GUI.h"
#include "Debugger.h"

class AssimpApp : public Application
{
public:
	AssimpApp(char* a_title, int a_screenWidth, int a_screenHeight);
	~AssimpApp();

	void Init();
	void Terminate();
	void ProcessInput();
	void Update();
	void Render();

private:
	Debugger m_Debugger;

	FlyCamera m_Camera;

	Shader* m_GridShader;
	Shader* m_TerrainShader;
	Shader* m_ModelShader;
	Shader* m_AnimationShader;
	GUI m_GUI;

	SceneObject m_Model;

	GPUTerrain m_Terrain;
	int m_Seed;
	bool m_IsDirty;
	float m_Amplitude;
	float m_Persistence;
	float m_Frequency;

	bool m_UseTrees;
};