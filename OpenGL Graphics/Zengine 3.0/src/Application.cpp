#include "gl_core_4_4.h"
#include "Application.h"
#include <assert.h>
#include "Input.h"
#include <GLM\glm.hpp>

Application::Application(char* a_title, int a_screenWidth, int a_screenHeight) :
	m_bIsRunning(true),
	m_strTitle(a_title),
	m_iScreenWidth(a_screenWidth),
	m_iScreenHeight(a_screenHeight),
	m_Window(nullptr),
	m_dDeltaTime(0),
	m_dTotalTime(0),
	m_uiGridVAO(0)
{
}
Application::~Application()
{

}

void Application::SystemStartUp()
{
	
	glfwInit();
	//ogl is broken
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	m_Window = glfwCreateWindow(m_iScreenWidth, m_iScreenHeight, m_strTitle, nullptr, nullptr);
	assert(m_Window != nullptr);
	glfwMakeContextCurrent(m_Window);

	int error = ogl_LoadFunctions();
	assert(error == ogl_LOAD_SUCCEEDED);

	glViewport(0, 0, m_iScreenWidth, m_iScreenHeight);
	glClearColor(0.8f, 0.8f, 1.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_dTotalTime = glfwGetTime();
	Input::Create();
	Input::Get()->SetMousePosition(m_Window, m_iScreenWidth / 2.0f, m_iScreenHeight / 2.0f);
	//Resource.init;
	glfwSetKeyCallback(m_Window, Input::KeyCallBack);
	glfwSetCursorPosCallback(m_Window, Input::CursorCallBack);
	glfwSetMouseButtonCallback(m_Window, Input::MouseCallBack);
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	Init();
}
void Application::SystemShutDown()
{
	Terminate();
	m_ResourceManager.Destroy();
	Input::Destroy();
	glfwTerminate();
}
void Application::Run()
{
	SystemStartUp();
	while (m_bIsRunning && !glfwWindowShouldClose(m_Window))
	{
		double currentTime = glfwGetTime();
		m_dDeltaTime = currentTime - m_dTotalTime;
		m_dTotalTime = currentTime;

		Input::Get()->UpdateInputs();
		glfwPollEvents();
		ProcessInput();
		Update();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Render();
		glfwSwapBuffers(m_Window);
		if (Input::Get()->KeyDown(GLFW_KEY_ESCAPE))
		{
			m_bIsRunning = false;
		}
	}
	SystemShutDown();
}

void Application::CreateGrid(unsigned int a_rows, unsigned int a_cols)
{
	if (!a_rows && !a_cols)
		return;

	if (m_uiGridVAO)
		glDeleteVertexArrays(1, &m_uiGridVAO);

	struct Vertex
	{
		glm::vec4 Position;
		glm::vec4 Colour;
	};
	
	m_uiRows = a_rows;
	m_uiCols = a_cols;

	int avg = (a_rows + a_cols) / 2;
	int x = (int)a_rows;
	int y = (int)a_cols;

	Vertex* vertices = new Vertex[(a_rows + a_cols + 1) * 4];
	unsigned int counter = 0;
	glm::vec4 white(1);
	glm::vec4 black(0, 0, 0, 1);
	for (int i = 0; i < (a_rows + a_cols + 1); ++i)
	{
		vertices[counter].Position = glm::vec4(-x + i, 0, y, 1);
		vertices[counter].Colour = (i == avg ? white : black);
		++counter;

		vertices[counter].Position = glm::vec4(-x + i, 0, -y, 1);
		vertices[counter].Colour = (i == avg ? white : black);
		++counter;

		vertices[counter].Position = glm::vec4(x, 0, -y + i, 1);
		vertices[counter].Colour = (i == avg ? white : black);
		++counter;

		vertices[counter].Position = glm::vec4(-x, 0, -y + i, 1);
		vertices[counter].Colour = (i == avg ? white : black);
		++counter;
	}

	unsigned int VBO;

	glGenVertexArrays(1, &m_uiGridVAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(m_uiGridVAO);
	//VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, ((a_rows + a_cols + 1) * 4) * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)(sizeof(glm::vec4)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	delete[] vertices;
}
void Application::DrawGrid(Shader shader)
{
	assert(m_uiGridVAO);
	shader.Use();
	glBindVertexArray(m_uiGridVAO);
	glDrawArrays(GL_LINES, 0, (m_uiRows + m_uiCols + 1) * 4);
	glBindVertexArray(0);
}
void Application::DestroyGrid()
{
	if (m_uiGridVAO)
		glDeleteVertexArrays(1, &m_uiGridVAO);
}