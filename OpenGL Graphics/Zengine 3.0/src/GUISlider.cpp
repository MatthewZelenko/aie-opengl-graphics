#include "GUISlider.h"
#include "Input.h"

GUISlider::GUISlider() : GUIElement(SLIDER)
{
	m_FillValue = 1.0f;
	m_Min = 0.0f;
	m_Max = 1.0f;
	m_FillPointer = nullptr;
	m_IsDirty = nullptr;
}
GUISlider::~GUISlider()
{
}

void GUISlider::LoadFillTexture(Texture* texture)
{
	m_FillTexture = texture;
}
void GUISlider::LoadBackTexture(Texture* texture)
{
	m_BackTexture = texture;
}

void GUISlider::ProcessInput()
{
	if (Input::Get()->MouseDown(GLFW_MOUSE_BUTTON_1))
	{
		double mX = Input::Get()->GetMousePosX();
		double mY = Input::Get()->GetMousePosY();
		float pX = m_FillPosition.x + m_Position.x;
		float pY = m_FillPosition.y + m_Position.y;
		float pW = m_FillSize.x + m_Size.x;
		float pH = m_FillSize.y + m_Size.y;

		if (mX > pX &&
			mY > pY &&
			mX < pX + pW &&
			mY < pY + pH)
		{
			SetFillValue(((mX - pX)) / pW * (m_Max - m_Min));
		}
	}
}

void GUISlider::SetFillValue(float value)
{
	value = glm::clamp(value, m_Min, m_Max);
	m_FillValue = ((1.0f) / (m_Max - m_Min)) * (value - m_Min);
	//float diff = (m_Max - m_Min) * m_FillValue;
	if(m_FillPointer)
		*m_FillPointer = m_Min + value;
	if (m_IsDirty)
		*m_IsDirty = true;
}
void GUISlider::SetBackPosition(glm::vec2 position)
{
	m_BackPosition = position;
}
void GUISlider::SetFillPosition(glm::vec2 position)
{
	m_FillPosition = position;
}
void GUISlider::SetBackSize(glm::vec2 size)
{
	m_BackSize = size;
}
void GUISlider::SetFillSize(glm::vec2 size)
{
	m_FillSize = size;
}
void GUISlider::SetVariablePointer(float* a_variable, float a_min, float a_max, bool* a_isDirty)
{
	if (a_isDirty)
		m_IsDirty = a_isDirty;
	m_Min = a_min;
	m_Max = a_max;
	if (a_variable)
	{
		m_FillPointer = a_variable;
		SetFillValue(*a_variable);
	}
}