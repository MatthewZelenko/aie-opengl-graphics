#include "Terrain.h"
#include "gl_core_4_4.h"
#include "Perlin.h"

Terrain::Terrain() :m_VAO(0), m_VBO(0), m_EBO(0)
{
}
Terrain::~Terrain()
{
}
void Terrain::Destroy()
{
	for (auto iter : m_Textures)
	{
		iter.Destroy();
	}
	if (m_VAO)
		glDeleteVertexArrays(1, &m_VAO);
	if (m_VBO)
		glDeleteBuffers(1, &m_VBO);
	if (m_EBO)
		glDeleteBuffers(1, &m_EBO);
}

void Terrain::GenerateTerrain(int a_rows, int a_cols, int a_seed, float a_amplitude , float a_frequency, float a_persistence)
{
	m_Vertices.clear();
	m_uiIndices.clear();
	m_iRows = a_rows;
	m_iCols = a_cols;
	m_iSeed = a_seed;
	m_fAmplitude = a_amplitude;
	m_fFrequency = a_frequency;
	m_fPersistence = a_persistence;
	Destroy();
	GenerateGrid();
	GeneratePerlin();
	GenerateNormals();

	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(TerrainVertex), &m_Vertices[0], GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_uiIndices.size() * sizeof(unsigned int), &m_uiIndices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(TerrainVertex), (void*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(TerrainVertex), (void*)offsetof(TerrainVertex, Normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(TerrainVertex), (void*)offsetof(TerrainVertex, TexCoord));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}
void Terrain::RegenerateTerrain(int a_seed)
{
	m_iSeed = a_seed;
	GeneratePerlin();
	GenerateNormals();

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_Vertices.size() * sizeof(TerrainVertex), &m_Vertices[0]);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

}
void Terrain::GenerateGrid()
{
	float texCoordX = 1.0f / (m_iRows - 1.0f);
	float texCoordY = 1.0f / (m_iCols - 1.0f);
	m_Vertices.reserve(m_iRows * m_iCols);
	for (int z = 0; z < m_iCols; ++z)
	{
		for (int x = 0; x < m_iRows; ++x)
		{
			TerrainVertex vertex;
			vertex.Position.x = (float)x - m_iRows / 2.0f;
			vertex.Position.z = (float)z - m_iCols / 2.0f;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x;
			vertex.TexCoord.y = (float)z;
			m_Vertices.push_back(vertex);
		}
	}

	int counter = 0;
	m_uiIndices.resize((m_iRows - 1) * (m_iCols - 1) * 6);
	for (int z = 0; z < m_iCols - 1; ++z)
	{
		for (int x = 0; x < m_iRows - 1; ++x)
		{
			m_uiIndices[counter++] = z * m_iRows + x;
			m_uiIndices[counter++] = z * m_iRows + (x + 1);
			m_uiIndices[counter++] = (z + 1) * m_iRows + (x + 1);

			m_uiIndices[counter++] = (z + 1) * m_iRows + (x + 1);
			m_uiIndices[counter++] = (z + 1) * m_iRows + x;
			m_uiIndices[counter++] = z * m_iRows + x;
		}
	}
}
void Terrain::GeneratePerlin()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (int x = 0; x < m_iRows; ++x)
	{
		for (int y = 0; y < m_iCols; ++y)
		{
			m_Vertices[y * m_iRows + x].Position.y = Perlin::GenPerlinNoise2((float)x + m_iSeed, (float)y + m_iSeed, m_fAmplitude, m_fFrequency, m_fPersistence) + m_fAmplitude / 2.0f;
		}
	}
}
void Terrain::GenerateNormals()
{
	int size = m_Vertices.size();
	for (int i = 0; i < size; ++i)
	{
		int x = i % m_iRows;
		int z = i / m_iRows;
		glm::vec3 average;
		glm::vec3 position = m_Vertices[i].Position;
		std::vector<glm::vec3> vectors;

		//up vector
		if (z > 0)
			vectors.push_back(m_Vertices[(z - 1) * m_iRows + x].Position - position);
		//right vector
		if (x < m_iRows - 1)
			vectors.push_back(m_Vertices[z * m_iRows + (x + 1)].Position - position);
		//down vector
		if (z < m_iCols - 1)
			vectors.push_back(m_Vertices[(z + 1) * m_iRows + x].Position - position);
		//left vector
		if (x > 0)
			vectors.push_back(m_Vertices[z * m_iRows + (x - 1)].Position - position);

		for (int j = 0; j < vectors.size() - 1; ++j)
		{
			glm::vec3 v1 = vectors[j];
			glm::vec3 v2 = vectors[j + 1];

			glm::vec3 vCross = glm::cross(v2, v1);

			average += vCross;
		}
		average /= (vectors.size() - 1);
		if (average.length() > 0)
			average = glm::normalize(average);
		m_Vertices[i].Normal = average;
	}
	//invert last normal
	m_Vertices[size - 1].Normal *= -1;
}

void Terrain::AddTextures(char* path)
{
	Texture texture;
	texture.CreateFromFile(path);
	m_Textures.push_back(texture);
}
void Terrain::Draw(Shader a_shader)
{
	if (m_VAO)
	{
		glBindVertexArray(m_VAO);
		if (m_Textures.size())
		{
			glActiveTexture(GL_TEXTURE0);
			m_Textures[0].Bind();
			if (m_Textures.size() > 1)
			{
				glActiveTexture(GL_TEXTURE1);
				m_Textures[1].Bind();
				if (m_Textures.size() > 2)
				{
					glActiveTexture(GL_TEXTURE2);
					m_Textures[2].Bind();
				}
			}
		}

		glDrawElements(GL_TRIANGLES, this->m_uiIndices.size(), GL_UNSIGNED_INT, 0);
	}
	glActiveTexture(GL_TEXTURE0);
}