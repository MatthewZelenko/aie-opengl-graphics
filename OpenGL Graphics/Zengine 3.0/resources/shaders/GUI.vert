#version 330 core

layout(location=0)in vec2 Position;
layout(location=1)in vec2 TextureCoords;

uniform mat4 Projection;
uniform mat4 Model;

out vec2 vTextureCoords;

void main()
{
	vTextureCoords = TextureCoords;
	gl_Position = Projection * Model * vec4(Position, 0, 1);
}