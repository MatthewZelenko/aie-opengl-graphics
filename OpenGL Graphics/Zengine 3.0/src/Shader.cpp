#include "Shader.h"
#include "gl_core_4_4.h"
#include <GLM\gtc\type_ptr.hpp>
#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader() :
	m_ID(0),
	m_strName("")
{
}
Shader::~Shader()
{
}
void Shader::Destroy()
{
	if (m_ID)
		glDeleteProgram(m_ID);
}

Shader Shader::CreateFromString(const char* a_vertex, const char* a_geometry, const char* a_fragment, const char* a_varyings[], const int a_varyingsSize)
{
	int success = 0;
	char infoLog[512];
	unsigned int vertexID = 0;
	unsigned int fragmentID = 0;
	unsigned int geometryID = 0;

	//Vertex Shader
	if (a_vertex)
	{
		vertexID = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexID, 1, &a_vertex, 0);
		glCompileShader(vertexID);

		glGetShaderiv(vertexID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexID, 512, 0, infoLog);
			std::cout << "Vertex: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}
	//Fragment Shader
	if (a_fragment)
	{
		fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentID, 1, &a_fragment, 0);
		glCompileShader(fragmentID);
		glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentID, 512, 0, infoLog);
			std::cout << "Fragment: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}
	//geometry
	if (a_geometry)
	{
		geometryID = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometryID, 1, &a_geometry, 0);
		glCompileShader(geometryID);
		glGetShaderiv(geometryID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(geometryID, 512, 0, infoLog);
			std::cout << "Geometry: Could not create shader (" << infoLog << ")" << std::endl;
		}
	}

	//program
	m_ID = glCreateProgram();
	if (vertexID)
		glAttachShader(m_ID, vertexID);
	if (fragmentID)
		glAttachShader(m_ID, fragmentID);
	if (geometryID)
		glAttachShader(m_ID, geometryID);

	if(a_varyingsSize > 0 && a_varyings != nullptr)
		glTransformFeedbackVaryings(m_ID, a_varyingsSize, a_varyings, GL_INTERLEAVED_ATTRIBS);

	glLinkProgram(m_ID);
	glGetProgramiv(m_ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(m_ID, 512, 0, infoLog);
		std::cout << "Program: Could not link shaders (" << infoLog << ")" << std::endl;
	}

	//dump
	if (vertexID)
		glDeleteShader(vertexID);
	if (fragmentID)
		glDeleteShader(fragmentID);
	if (geometryID)
		glDeleteShader(geometryID);
	assert(success && "Shader creation did not succeed");
	return *this;
}
Shader Shader::CreateFromFile(const char* a_vertexPath, const char* a_geometryPath, const char* a_fragmentPath, const char* a_varyings[], const int a_varyingsSize)
{
	std::string vertexString, fragmentString, geometryString;

	if (a_vertexPath)
	{
		std::ifstream vertexFile;
		std::stringstream vertexStream;

		vertexFile.open(a_vertexPath);
		assert(vertexFile.good() && "Shader::Vertex: Could not read file");

		vertexStream << vertexFile.rdbuf();
		vertexString = vertexStream.str();

		vertexFile.close();
	}
	if (a_fragmentPath)
	{
		std::ifstream fragmentFile;
		std::stringstream fragmentStream;

		fragmentFile.open(a_fragmentPath);
		assert(fragmentFile.good() && "Shader::Fragment: Could not read file");

		fragmentStream << fragmentFile.rdbuf();
		fragmentString = fragmentStream.str();

		fragmentFile.close();
	}
	if (a_geometryPath)
	{
		std::ifstream geometryFile;
		std::stringstream geometryStream;

		geometryFile.open(a_geometryPath);
		assert(geometryFile.good() && "Shader::Geometry: Could not read file");

		geometryStream << geometryFile.rdbuf();
		geometryString = geometryStream.str();

		geometryFile.close();
	}
	const char* vertexCode = nullptr;
	const char* fragmentCode = nullptr;
	const char* geometryCode = nullptr;
	if (vertexString != "")
		vertexCode = vertexString.c_str();
	if (fragmentString != "")
		fragmentCode = fragmentString.c_str();
	if (geometryString != "")
		geometryCode = geometryString.c_str();

	CreateFromString(vertexCode, geometryCode, fragmentCode, a_varyings, a_varyingsSize);
	return *this;
}

void Shader::Use()
{
	assert(m_ID);
	glUseProgram(m_ID);
}

void Shader::SendInt(char* a_location, int a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	//assert(loc >= 0 && "Could not find shader location");
	glUniform1i(loc, a_value);
}
void Shader::SendUint(char* a_location, unsigned int a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniform1ui(loc, a_value);
}
void Shader::SendFloat(char* a_location, float a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniform1f(loc, a_value);
}
void Shader::SendBool(char* a_location, bool a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniform1i(loc, a_value);
}
void Shader::SendMat4(char* a_location, int a_count, glm::mat4& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniformMatrix4fv(loc, a_count, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendMat3(char* a_location, int a_count, glm::mat3& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniformMatrix3fv(loc, a_count, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendMat2(char* a_location, int a_count, glm::mat2& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniformMatrix2fv(loc, a_count, GL_FALSE, glm::value_ptr(a_value));
}
void Shader::SendVec4(char* a_location, int a_count, glm::vec4& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniform4fv(loc, a_count, glm::value_ptr(a_value));
}
void Shader::SendVec3(char* a_location, int a_count, glm::vec3& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniform3fv(loc, a_count, glm::value_ptr(a_value));
}
void Shader::SendVec2(char* a_location, int a_count, glm::vec2& a_value)
{
	assert(m_ID);
	int loc = glGetUniformLocation(m_ID, a_location);
	assert(loc >= 0 && "Could not find shader location");
	glUniform2fv(loc, a_count, glm::value_ptr(a_value));
}