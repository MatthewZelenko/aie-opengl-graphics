![Banner](https://bytebucket.org/MatthewZelenko/opengl-graphics/raw/827b01543bdf3b17f8709e8a4ac1139640a7e688/Graphics%20Banner.jpg)

###OpenGL Graphics Project###
This was created using Visual Studio 2015.

Summary:
This project demonstrates the use of OpenGL. It also comes with the features of GPU Terrain Generation, transform feedback for tree placement, multiple textures, and water.
The Controls are inside. 

Requirements:
Download, open in Visual Studio, run.
Alternatively you can download the build [here](https://drive.google.com/uc?export=download&id=0BzmWDCip_PJLQmFXcWNBVVNYMUU)