#pragma once
#include <GLM\glm.hpp>
#include <vector>
#include <string>

class Shader
{
public:
	Shader();
	~Shader();
	/*Destroys program*/
	void Destroy();

	/*Creates a program using shader strings and also sets feedback varyings*/
	Shader CreateFromString(const char* a_vertex, const char* a_geometry, const char* a_fragment, const char* a_varyings[] = nullptr, const int a_varyingsSize = 0);
	/*Creates a program from shader files and also sets feedback varyings*/
	Shader CreateFromFile(const char* a_vertex, const char* a_geometry, const char* a_fragment, const char* a_varyings[] = nullptr, const int a_varyingsSize = 0);

	/*Needed to use program*/
	void Use();

	/*Sends an int to the shaders uniform location.
	Requires Use function first*/
	void SendInt(char* a_location, int a_value);
	/*Sends an unsigned int to the shaders uniform location.
	Requires Use function first*/
	void SendUint(char* a_location, unsigned int a_value);
	/*Sends a float to the shaders uniform location.
	Requires Use function first*/
	void SendFloat(char* a_location, float a_value);
	/*Sends a bool to the shaders uniform location.
	Requires Use function first*/
	void SendBool(char* a_location, bool a_value);
	/*Sends mat4s to the shaders uniform location.
	Requires Use function first*/
	void SendMat4(char* a_location, int a_count, glm::mat4& a_value);
	/*Sends mat3s to the shaders uniform location.
	Requires Use function first*/
	void SendMat3(char* a_location, int a_count, glm::mat3& a_value);
	/*Sends mat2s to the shaders uniform location.
	Requires Use function first*/
	void SendMat2(char* a_location, int a_count, glm::mat2& a_value);
	/*Sends vec4s to the shaders uniform location.
	Requires Use function first*/
	void SendVec4(char* a_location, int a_count, glm::vec4& a_value);
	/*Sends vec3s to the shaders uniform location.
	Requires Use function first*/
	void SendVec3(char* a_location, int a_count, glm::vec3& a_value);
	/*Sends vec2s to the shaders uniform location.
	Requires Use function first*/
	void SendVec2(char* a_location, int a_count,  glm::vec2& a_value);

	/*Sets the name of the program*/
	void SetName(char* a_name) { m_strName = a_name; }

	/*Returns program ID*/
	int GetID() { return m_ID; }

	/*Returns name of program*/
	char* GetName() { return m_strName; }

protected:
	char* m_strName;
	unsigned int m_ID;
};