#include "AssimpApp.h"
#include "Input.h"

AssimpApp::AssimpApp(char* a_title, int a_screenWidth, int a_screenHeight):
	Application(a_title, a_screenWidth, a_screenHeight), m_Seed(0), m_Amplitude(0.0f), m_Persistence(0), m_Frequency(0), m_IsDirty(true), m_UseTrees(true)
{
}
AssimpApp::~AssimpApp()
{
}

void AssimpApp::Init()
{

	srand(time_t(NULL));
	m_Amplitude = 220.0f;
	m_Frequency = 0.005f;
	m_Persistence = 0.5f;

	//camera
	m_Camera = FlyCamera(m_Window, 100, 0.002f);
	m_Camera.SetInputKeys(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_MOUSE_BUTTON_2);
	m_Camera.SetPerspective(45.0f, (float)GetScreenWidth() / (float)GetScreenHeight(), 0.1f, 1000000.0f);
	m_Camera.SetView(glm::vec3(0, 200,200), glm::vec3(0, -1, -1), glm::vec3(0, 1, 0));
	//////////////////////////////
	m_Debugger.Init(m_Camera.GetProjection());
	//Resources
	Texture* sliderBack = m_ResourceManager.LoadTexture("resources/images/GUISliderBack.png");
	Texture* sliderFill = m_ResourceManager.LoadTexture("resources/images/GUISliderFill.png");
	Texture* bgTexture = m_ResourceManager.LoadTexture("resources/images/GUIImage.png");

	Model* tree = m_ResourceManager.LoadModel("resources/models/tree/Tree.fbx");

	//GUI
	m_GUI.Init((float)GetScreenWidth(), (float)GetScreenHeight());
		//Slider
		GUISlider* slider = new GUISlider();
		
			//Amp slider
			slider->LoadBackTexture(sliderBack);
			slider->LoadFillTexture(sliderFill);
			slider->SetPosition(glm::vec2(10, 50));
			slider->SetBackSize(glm::vec2(180, 16));
			slider->SetFillSize(glm::vec2(168, 8));
			slider->SetFillPosition(glm::vec2(6, 4));
			slider->SetVariablePointer(&m_Amplitude, 0.0f, 500.0f, &m_IsDirty);
			slider->SetFillValue(m_Amplitude);
			m_GUI.AddGUI(slider);
			
			//Frequency slider
			slider = new GUISlider();
			slider->LoadBackTexture(sliderBack);
			slider->LoadFillTexture(sliderFill);
			slider->SetPosition(glm::vec2(10, 100));
			slider->SetBackSize(glm::vec2(180, 16));
			slider->SetFillSize(glm::vec2(168, 8));
			slider->SetFillPosition(glm::vec2(6, 4));
			slider->SetVariablePointer(&m_Frequency, 0.0f, 0.02f, &m_IsDirty);
			slider->SetFillValue(m_Frequency);
			m_GUI.AddGUI(slider);

			//Persistence slider
			slider = new GUISlider();
			slider->LoadBackTexture(sliderBack);
			slider->LoadFillTexture(sliderFill);
			slider->SetPosition(glm::vec2(10, 150));
			slider->SetBackSize(glm::vec2(180, 16));
			slider->SetFillSize(glm::vec2(168, 8));
			slider->SetFillPosition(glm::vec2(6, 4));
			slider->SetVariablePointer(&m_Persistence, 0.0f, 0.7f, &m_IsDirty);
			slider->SetFillValue(m_Persistence);
			m_GUI.AddGUI(slider);

			//GUI BG
			GUIImage* bg = new GUIImage();
			bgTexture->SetMinFilter(GL_NEAREST);
			bgTexture->SetMagFilter(GL_NEAREST);
			bg->LoadTexture(bgTexture);
			bg->SetPosition(glm::vec2(0));
			bg->SetSize(glm::vec2(200,400));
			m_GUI.AddGUI(bg);
	//////////////////////////////
	//Grid
	m_GridShader = m_ResourceManager.LoadShader("grid", "resources/shaders/grid.vert", nullptr, "resources/shaders/grid.frag");
	m_GridShader->Use();
	m_GridShader->SendMat4("Projection", 1, m_Camera.GetProjection());
	m_GridShader->SendMat4("View", 1, m_Camera.GetView());
	CreateGrid(64, 64);
	//////////////////////////////
	//Model
	m_ModelShader = m_ResourceManager.LoadShader("model", "resources/shaders/Mesh.vert", nullptr, "resources/shaders/Mesh.frag");
	m_ModelShader->Use();
	m_ModelShader->SendMat4("Projection", 1, m_Camera.GetProjection());
	m_ModelShader->SendMat4("View", 1, m_Camera.GetView());
	m_ModelShader->SendInt("Diffuse", 0);
	m_ModelShader->SendInt("Specular", 1);
	//////////////////////////////
	//Animation
	m_AnimationShader = m_ResourceManager.LoadShader("animation", "resources/shaders/Animation.vert", nullptr, "resources/shaders/Animation.frag");
	m_AnimationShader->Use();
	m_AnimationShader->SendMat4("Projection", 1, m_Camera.GetProjection());
	m_AnimationShader->SendMat4("View", 1, m_Camera.GetView());
	m_AnimationShader->SendInt("Diffuse", 0);
	m_AnimationShader->SendInt("Specular", 1);

	m_Model.SetModel(m_ResourceManager.LoadModel("resources/models/Demolition/demolition.fbx"));
	m_Model.SetSize(glm::vec3(3.0f));
	//////////////////////////////

	//Terrain
	m_TerrainShader = m_ResourceManager.LoadShader("terrain", "resources/shaders/Terrain.vert", nullptr, "resources/shaders/Terrain.frag");
	m_TerrainShader->Use();
	m_TerrainShader->SendMat4("Projection", 1, m_Camera.GetProjection());
	m_TerrainShader->SendMat4("View", 1, m_Camera.GetView());
	m_TerrainShader->SendInt("DirtTexture", 0);
	m_TerrainShader->SendInt("RockTexture", 1);
	m_TerrainShader->SendInt("SnowTexture", 2);
	m_TerrainShader->SendInt("WaterTexture", 3);
	m_TerrainShader->SendFloat("Amplitude", 100.0f);

	m_Terrain.Init(500, 500, 8.0f, 8.0f, m_Seed, m_Amplitude, m_Frequency, m_Persistence, 8);
	m_Terrain.AddTreeModel(tree);
	m_Terrain.AddTextures(m_ResourceManager.LoadTexture("resources/images/grass.jpg"));
	m_Terrain.AddTextures(m_ResourceManager.LoadTexture("resources/images/dirt.jpg"));
	m_Terrain.AddTextures(m_ResourceManager.LoadTexture("resources/images/snow.jpg"));
	Texture* waterTexture = m_ResourceManager.LoadTexture("resources/images/water.jpg");
	m_Terrain.AddTextures(waterTexture);
	m_Terrain.SetMaxTreeSteep(0.9f);
	m_Terrain.SetMinAndMaxTreeSize(50.0f, 100.0f);
	m_Terrain.SetMinAndMaxTreeYPosition(-15.0f, 35);
	m_Terrain.SetTreeSpawnChance(1);
	m_Terrain.SetWaterTexture(waterTexture);
	m_Terrain.SetWaterYPosition(-15.0f);
	m_Terrain.GenerateWater();
	//////////////////////////////
}
void AssimpApp::Terminate()
{
	m_GUI.Destroy();
	m_Terrain.Destroy();
}
void AssimpApp::ProcessInput()
{
	//std::printf("%f\n", m_dDeltaTime);
	m_GUI.ProcessInput();
	if (Input::Get()->KeyDown(GLFW_KEY_EQUAL))
	{
		m_Seed += 20;
		m_IsDirty = true;
	}
	if (Input::Get()->KeyDown(GLFW_KEY_MINUS))
	{
		m_Seed -= 20;
		m_IsDirty = true;
	}
	if (Input::Get()->KeyPressed(GLFW_KEY_BACKSPACE))
	{
		m_UseTrees = !m_UseTrees;
		if (m_UseTrees)
			m_Terrain.GenerateTrees();
	}
	if (m_IsDirty)
	{
		m_Terrain.SetAmplitude(m_Amplitude);
		m_Terrain.SetFrequency(m_Frequency);
		m_Terrain.SetPersistence(m_Persistence);
		m_Terrain.GenerateTerrain(m_Seed);
		if(m_UseTrees)
			m_Terrain.GenerateTrees();		
		m_IsDirty = false;
	}
}
void AssimpApp::Update()
{
	m_Terrain.GenerateWater();
	m_Camera.Update(m_dDeltaTime);

	FBXSkeleton* skeleton = m_Model.GetModel()->GetFBX()->getSkeletonByIndex(0);
	FBXAnimation* animation = m_Model.GetModel()->GetFBX()->getAnimationByIndex(0);

	static float elapsed = 920 / 24.0f;
	elapsed += m_dDeltaTime;
	if (elapsed >= 944 / 24.0f)
		elapsed = 920 / 24.0f;
	skeleton->evaluate(animation, elapsed, true);

	for (unsigned int i = 0; i < skeleton->m_boneCount; ++i)
	{
		skeleton->m_nodes[i]->updateGlobalTransform();
	}
}
void AssimpApp::Render()
{
	m_Debugger.ClearObjects();
	glm::vec3 lightPos = glm::vec3(cos(glfwGetTime() / 10.0f) * 10000, sin(glfwGetTime() / 10.0f) * 10000, 0);
	glm::vec3 lightColour = glm::vec3(1.0f, 0.6f, 0.6f);
	if (m_Camera.GetPosition().y < -15.0f)
	{
		lightColour = glm::vec3(0.75f, 0.5f, 1.0f);
	}

	glm::vec4 skyColour(0.8f, 0.8f, 1.0f, 1.0f);
	skyColour *= glm::mix(0.25f, 1.0f, (sin((float)glfwGetTime() / 10.0f)));

	SetClearColour(skyColour);
	m_Debugger.AddSphere(lightPos, 750.0f, glm::vec4(lightColour, 1.0f));
	m_Debugger.Draw(m_Camera.GetView());
	//grid
	//m_GridShader->Use();
	//m_GridShader->SendMat4("View", 1, m_Camera.GetView());
	//DrawGrid(*m_GridShader);
	//////////////////////////////

	//Terrain
	m_TerrainShader->Use();
	m_TerrainShader->SendMat4("View", 1, m_Camera.GetView());
	m_TerrainShader->SendVec3("CameraPos", 1, m_Camera.GetPosition());
	m_TerrainShader->SendVec3("LightPos", 1, lightPos);
	m_TerrainShader->SendVec3("LightColour", 1, lightColour);
	m_Terrain.DrawTerrain(*m_TerrainShader, &m_Camera);
	
	if (m_Camera.GetPosition().y < -15.0f)
	{
		m_AnimationShader->Use();
		m_AnimationShader->SendMat4("View", 1, m_Camera.GetView());
		m_AnimationShader->SendVec3("CameraPos", 1, m_Camera.GetPosition());
		m_AnimationShader->SendVec3("LightPos", 1, lightPos);
		m_AnimationShader->SendVec3("LightColour", 1, lightColour);
		m_Model.Draw(*m_AnimationShader);
	}

	m_ModelShader->Use();
	m_ModelShader->SendMat4("View", 1, m_Camera.GetView());
	m_ModelShader->SendVec3("CameraPos", 1, m_Camera.GetPosition());
	m_ModelShader->SendVec3("LightPos", 1, lightPos);
	m_ModelShader->SendVec3("LightColour", 1, lightColour);
	m_Terrain.DrawWater(*m_ModelShader, &m_Camera);
	if (m_UseTrees)
		m_Terrain.DrawTrees(*m_ModelShader, &m_Camera);

	//////////////////////////////
	if (m_Camera.GetPosition().y >= -15.0f)
	{

		m_AnimationShader->Use();
		m_AnimationShader->SendMat4("View", 1, m_Camera.GetView());
		m_AnimationShader->SendVec3("CameraPos", 1, m_Camera.GetPosition());
		m_AnimationShader->SendVec3("LightPos", 1, lightPos);
		m_AnimationShader->SendVec3("LightColour", 1, lightColour);
		m_Model.Draw(*m_AnimationShader);
	}
	//GUI
	m_GUI.Render();
	/////////////////////////////
}