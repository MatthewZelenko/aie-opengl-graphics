#pragma once
#include <GLM\glm.hpp>
#include "Shader.h"
#include <vector>

class Debugger
{
public:
	Debugger();
	~Debugger();
	
	void ClearObjects();

	/*Creates debugger shader and sphere buffers.*/
	void Init(glm::mat4& a_projection);

	/*Creates a grid.*/
	void CreateGrid(unsigned int a_rows, unsigned int a_cols, glm::vec4& a_colour = glm::vec4(1.0f));
	/*Draws a sphere.*/
	void AddSphere(glm::vec3& a_center, float a_radius, glm::vec4& a_colour);
	
	/*Draws all debugger objects.*/
	void Draw(glm::mat4& a_view);
protected:
	/*Draws grid. Called in Draw function.*/
	void DrawGrid();
	/*Draws spheres. Called in Draw function.*/
	void DrawSpheres();
	/*Creates a base sphere buffer.*/
	void CreateSphere();

	Shader m_Shader;

	struct Vertex 
	{
		glm::vec4 Position;
		glm::vec4 Colour;
	};
	//Grid
	unsigned int m_GridVAO;
	unsigned int m_GridRows, m_GridCols;
	glm::vec4 m_GridColour;
	//Sphere
	struct DebuggerSphere
	{
		glm::vec3 Center;
		float Radius;
		glm::vec4 Colour;
	};
	unsigned int m_SphereVAO;
	std::vector<DebuggerSphere> m_Spheres;
	unsigned int m_SphereIndexSize;
};