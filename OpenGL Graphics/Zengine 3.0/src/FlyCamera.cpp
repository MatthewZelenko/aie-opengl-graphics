#include "FlyCamera.h"
#include "Input.h"

FlyCamera::FlyCamera()
{
	m_ForwardKey = -1;
	m_BackwardKey = -1;
	m_LeftKey = -1;
	m_RightKey = -1;
	m_MoveSpeed = 0;
	m_MouseSensitivity = 0;
	m_Window = nullptr;
	m_ControlEnabled = false;
	m_ControlEnableKey = -1;
}
FlyCamera::FlyCamera(GLFWwindow* a_window, float a_moveSpeed, float a_mouseSensitivity)
{
	m_ForwardKey = -1;
	m_BackwardKey = -1;
	m_LeftKey = -1;
	m_RightKey = -1;
	m_MoveSpeed = a_moveSpeed;
	m_MouseSensitivity = a_mouseSensitivity;
	m_Window = a_window;
	m_ControlEnabled = false;
	m_ControlEnableKey = -1;
}
FlyCamera::~FlyCamera()
{

}

void FlyCamera::Update(float a_deltaTime)
{
	if (Input::Get()->MousePressed(m_ControlEnableKey))
	{
		m_ControlEnabled = !m_ControlEnabled;
		if (m_ControlEnabled)
		{
			glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
		else
		{
			glfwSetInputMode(m_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}
	if (m_ControlEnabled)
	{
		double xOffset = Input::Get()->GetMouseOffsetX();
		double yOffset = Input::Get()->GetMouseOffsetY();

		if (xOffset)
		{
			RotateAroundWorldUp((float)xOffset * m_MouseSensitivity);
		}
		if (yOffset)
		{
			float dot = glm::dot(m_Forward, glm::vec3(0.0f, -1.0f, 0.0f));
			if ((dot < -0.999f && yOffset > 0.0f) || (dot > 0.999f && yOffset < 0.0f))
			{
				yOffset = 0.0f;
			}
			RotateAroundRight((float)-yOffset * m_MouseSensitivity);
		}

		float x = 0;
		float z = 0;
		if (Input::Get()->KeyDown(m_ForwardKey))
		{
			z += m_MoveSpeed * a_deltaTime;
		}
		if (Input::Get()->KeyDown(m_BackwardKey))
		{
			z -= m_MoveSpeed * a_deltaTime;
		}
		if (Input::Get()->KeyDown(m_LeftKey))
		{
			x -= m_MoveSpeed * a_deltaTime;
		}
		if (Input::Get()->KeyDown(m_RightKey))
		{
			x += m_MoveSpeed * a_deltaTime;
		}
		if (x != 0)
			TranslateRight(x);
		if (z != 0)
			TranslateForward(z);
		SetUp(glm::vec3(0.0f, 1.0f, 0.0f));
	}
}
void FlyCamera::SetInputKeys(int a_forwardKey, int a_backwardKey, int a_leftKey, int a_rightKey, int a_controlEnableKey)
{
	m_ForwardKey = a_forwardKey;
	m_BackwardKey = a_backwardKey;
	m_LeftKey = a_leftKey;
	m_RightKey = a_rightKey;
	m_ControlEnableKey = a_controlEnableKey;
}
void FlyCamera::SetMouseSensitivity(float a_mouseSensitivity)
{
	m_MouseSensitivity = a_mouseSensitivity;
}
void FlyCamera::SetMoveSpeed(float a_moveSpeed)
{
	m_MoveSpeed = a_moveSpeed;
}