#pragma once
#include <GLFW\glfw3.h>
#include <Shader.h>
#include "ResourceManager.h"

class Application
{
public:
	Application(char* a_title, int a_screenWidth, int a_screenHeight);
	virtual ~Application();

	void Run();

protected:
	virtual void Init() = 0;
	virtual void Terminate() = 0;
	virtual void ProcessInput() = 0;
	virtual void Update() = 0;
	virtual void Render() = 0;

	void CreateGrid(unsigned int a_rows, unsigned int a_cols);
	void DrawGrid(Shader shader);
	void DestroyGrid();

	void SetClearColour(glm::vec4 colour) { glClearColor(colour.r, colour.g, colour.b, colour.a); }

	int GetScreenWidth() { return m_iScreenWidth; }
	int GetScreenHeight() { return m_iScreenHeight; }

	GLFWwindow* m_Window;

	double m_dDeltaTime;
	double m_dTotalTime;
	ResourceManager m_ResourceManager;

private:
	char* m_strTitle;

	void SystemStartUp();
	void SystemShutDown();

	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bIsRunning;

	//Grid
	unsigned int m_uiGridVAO;
	unsigned int m_uiRows;
	unsigned int m_uiCols;
};