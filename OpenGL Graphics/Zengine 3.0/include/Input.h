#pragma once
#include <GLFW\glfw3.h>
/*Contains Methods to check if keys or mouse buttons are held down, held up, pressed, or released*/
class Input
{
public:
	/*Used in base Application class' SystemStartUp function*/
	static void Create();
	/*Used in base Application class' SystemShutDown function*/
	static void Destroy();

	/*Used in conjunction with glfwSetKeyCallback.
	Used in base Application class' SystemInit function*/
	static void KeyCallBack(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods);
	/*Used in conjunction with glfwSetCursorPosCallback.
	Used in base Application class' SystemInit function*/
	static void CursorCallBack(GLFWwindow* a_window, double a_x, double a_y);
	/*Used in conjunction with glfwSetMouseButtonCallback.
	Used in base Application class' SystemInit function*/
	static void MouseCallBack(GLFWwindow* a_window, int a_button, int a_action, int a_mods);
	/*Used in base Application class' Run function*/
	void UpdateInputs();

	/*Returns true if a_key is down*/
	bool KeyDown(int a_key);
	/*Returns true if a_key is up*/
	bool KeyUp(int a_key);
	/*Returns true if a_key had just been pressed*/
	bool KeyPressed(int a_key);
	/*Returns true if a_key had just been released*/
	bool KeyReleased(int a_key);


	/*Returns true if a_button is down*/
	bool MouseDown(int a_button);
	/*Returns true if a_button is up*/
	bool MouseUp(int a_button);
	/*Returns true if a_button had just been pressed*/
	bool MousePressed(int a_button);
	/*Returns true if a_button had just been released*/
	bool MouseReleased(int a_button);
	//Sets mouse position
	void SetMousePosition(GLFWwindow* a_window, float x, float y);

	/*Returns mouses x position*/
	double GetMousePosX();
	/*Returns mouses y position*/
	double GetMousePosY();
	/*Returns difference between current frames mouses x position and last frames mouses x position*/
	double GetMouseOffsetX();
	/*Returns difference between current frames mouses y position and last frames mouses y position*/
	double GetMouseOffsetY();


	/*Returns Input singleton.
	Needed to access class functions*/
	static Input* Get();

private:
	Input();
	~Input();

	static Input* m_Instance;

	bool m_bKeys[1024];
	bool m_bOldKeys[1024];

	bool m_bMouseButton[8];
	bool m_bOldMouseButton[8];

	double m_dOldMousePosX;
	double m_dOldMousePosY;
	double m_dMousePosX;
	double m_dMousePosY;
};