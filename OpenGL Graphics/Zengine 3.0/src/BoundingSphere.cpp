#include "BoundingSphere.h"

BoundingSphere::BoundingSphere()
{
	m_Type = SPHERE;
	Reset();
}
BoundingSphere::~BoundingSphere()
{
}

void BoundingSphere::Reset()
{
	m_Min.x = m_Min.y = m_Min.z = 1e37f;
	m_Max.x = m_Max.y = m_Max.z = -1e37f;
}
void BoundingSphere::Fit(std::vector<FBXVertex>& a_points)
{
	for (auto& p : a_points)
	{
		//min
		if (p.position.x < m_Min.x) m_Min.x = p.position.x;
		if (p.position.y < m_Min.y) m_Min.y = p.position.y;
		if (p.position.z < m_Min.z) m_Min.z = p.position.z;
		//max
		if (p.position.x > m_Max.x) m_Max.x = p.position.x;
		if (p.position.y > m_Max.y) m_Max.y = p.position.y;
		if (p.position.z > m_Max.z) m_Max.z = p.position.z;
	}
	m_Center = (m_Min + m_Max) / 2.0f;
	m_Radius = glm::distance(m_Min, m_Center);
}

void BoundingSphere::SetCenter(glm::vec3 a_value)
{
	m_Center = a_value;
}
void BoundingSphere::SetRadius(float a_value)
{
	m_Radius = a_value;
}
void BoundingSphere::ScaleRadius(float a_scale)
{
	m_Radius *= a_scale;
}