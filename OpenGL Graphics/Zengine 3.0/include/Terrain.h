#pragma once
#include <GLM\glm.hpp>
#include <vector>
#include "Shader.h"
#include "Texture.h"

struct TerrainVertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoord;
};
class Terrain
{
public:
	Terrain();
	~Terrain();

	void Destroy();

	void GenerateTerrain(int a_rows, int a_cols, int a_seed, float a_amplitude = 1.0f, float a_frequency = 1.0f, float a_persistence = 0.5f);
	void RegenerateTerrain(int a_seed);
	void AddTextures(char* path);
	void Draw(Shader a_shader);

protected:
	void GenerateGrid();
	void GeneratePerlin();
	void GenerateNormals();

private:
	unsigned int m_VAO, m_VBO, m_EBO;

	std::vector<TerrainVertex> m_Vertices;
	std::vector<unsigned int> m_uiIndices;

	int m_iRows;
	int m_iCols;
	float m_fAmplitude;
	float m_fFrequency;
	float m_fPersistence;
	int m_iSeed;

	std::vector<Texture> m_Textures;
};