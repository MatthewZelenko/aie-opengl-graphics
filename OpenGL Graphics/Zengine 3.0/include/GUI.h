#pragma once
#include <GLM\glm.hpp>
#include "Shader.h"
#include <vector>
#include "GUIImage.h"
#include "GUISlider.h"

class GUI
{
public:
	GUI();
	~GUI();
	void Destroy();

	void Init(float windowWidth, float windowHeight);
	void AddGUI(GUIElement* gui);
	//void RemoveGUI(std::string name);

	void ProcessInput();
	void Render();

	//GUIElement* GetGUI(std::string name) { return m_GUIs[name]; }

protected:
	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_EBO;
	glm::mat4 m_Projection;

	Shader m_Shader;

	std::vector<GUIElement*> m_GUIs;
};