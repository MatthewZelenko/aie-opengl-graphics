#pragma once
#include "Shader.h"
#include "FBX\FBXFile.h"
#include <string>

class Model
{
public:
	Model();
	~Model();
	void Destroy();

	void LoadModel(const std::string& a_path);

	void Draw(Shader a_shader, glm::mat4& a_model);

	FBXFile* GetFBX() { return m_Scene; }

	unsigned int GetVAO(int index) { return m_VAOs.at(index); }
	unsigned int GetVBO(int index) { return m_VBOs.at(index); }
	unsigned int GetEBO(int index) { return m_EBOs.at(index); }


private:
	void CreateBuffers();
	std::string m_strPath;
	FBXFile* m_Scene;

	std::vector<unsigned int>m_VAOs;
	std::vector<unsigned int>m_VBOs;
	std::vector<unsigned int>m_EBOs;
};