#include "GUI.h"
#include "gl_core_4_4.h"
#include <GLM\gtc\matrix_transform.hpp>

GUI::GUI()
{
}
GUI::~GUI()
{
}
void GUI::Destroy()
{
	for (auto iter : m_GUIs)
	{
		delete iter;
	}
	m_Shader.Destroy();
	if (m_VAO)
		glDeleteVertexArrays(1, &m_VAO);
	if (m_VBO)
		glDeleteBuffers(1, &m_VBO);
	if (m_EBO)
		glDeleteBuffers(1, &m_EBO);
}

void GUI::Init(float windowWidth, float windowHeight)
{
	Destroy();

	float vertices[] = {
		0.0f, 0.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 1.0f, 0.0f,
		1.0f,  1.0f, 1.0f, 1.0f,
		0.0f,  1.0f, 0.0f, 1.0f,
	};
	unsigned int indices[] = {
		0, 3, 2,
		2, 1, 0
	};

	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (GLvoid*)(sizeof(float) * 2));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	m_Projection = glm::ortho(0.0f, windowWidth, windowHeight, 0.0f, -1.0f, 1.0f);

	m_Shader.CreateFromFile("../Zengine 3.0/Resources/Shaders/GUI.vert", nullptr, "../Zengine 3.0/Resources/Shaders/GUI.frag");
	m_Shader.SendMat4("Projection", 1, m_Projection);
}

void GUI::AddGUI(GUIElement* gui)
{
	m_GUIs.push_back(gui);
}
//void GUI::RemoveGUI()
//
//	if (m_GUIs[name])
//		delete m_GUIs[name];
//	m_GUIs[name] = nullptr;
//
void GUI::ProcessInput()
{
	for (auto &iter : m_GUIs)
	{
		iter->ProcessInput();
	}
}
void GUI::Render()
{
	if (m_VAO)
		glBindVertexArray(m_VAO);
	m_Shader.Use();

	for (auto &iter : m_GUIs)
	{
		if (iter->GetType() == IMAGE)
		{
			GUIImage* image = (GUIImage*)iter;

			glActiveTexture(GL_TEXTURE0);
			image->GetTexture().Bind();
			glm::mat4 model;
			model = glm::translate(model, glm::vec3(image->GetPosition(), 0));
			model = glm::scale(model, glm::vec3(image->GetSize(), 1));

			m_Shader.SendMat4("Model", 1, model);
			m_Shader.SendMat4("Projection", 1, m_Projection);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		}
		else if (iter->GetType() == SLIDER)
		{
			GUISlider* image = (GUISlider*)iter;
			glm::mat4 model;
			glActiveTexture(GL_TEXTURE0);

			//Fill
			model = glm::translate(glm::mat4(), glm::vec3(image->GetPosition() + image->GetFillPosition(), 0));
			model = glm::scale(model, glm::vec3((image->GetSize().x + image->GetFillSize().x) * image->GetFillValue(), image->GetSize().y + image->GetFillSize().y, 1));
			m_Shader.SendMat4("Model", 1, model);
			image->GetFillTexture().Bind();
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

			//Back
			model = glm::translate(glm::mat4(), glm::vec3(image->GetPosition() + image->GetBackPosition(), 0));
			model = glm::scale(model, glm::vec3(image->GetSize() + image->GetBackSize(), 1));
			m_Shader.SendMat4("Model", 1, model);
			image->GetBackTexture().Bind();
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		}
	}
	glBindVertexArray(0);
}
