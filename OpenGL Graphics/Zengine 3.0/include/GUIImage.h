#pragma once
#include "GUIElement.h"
#include "Texture.h"

class GUIImage :
	public GUIElement
{
public:
	GUIImage();
	~GUIImage();

	void LoadTexture(Texture* texture);

	Texture GetTexture() { return *m_Texture; }

private:
	Texture* m_Texture;
};

