#pragma once
#include "Texture.h"
#include "Shader.h"
#include "Model.h"
#include <map>

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	void Destroy();

	Texture* LoadTexture(char* a_filePath);
	Texture* GetTexture(char* a_filePath) { return &m_Textures[a_filePath]; }

	Shader* LoadShader(char* a_shaderName, char* a_vertFilePath, char* a_geomFilePath, char* a_fragFilePath, const char** a_varyings = nullptr, const int a_varyingsSize = 0);
	Shader* GetShader(char* a_filePath) { return &m_Shaders[a_filePath]; }

	Model* LoadModel(char* a_filePath);
	Model* GetModel(char* a_filePath) { return &m_Models[a_filePath]; }

private:
	std::map<std::string, Texture> m_Textures;
	std::map<std::string, Shader> m_Shaders;
	std::map<std::string, Model> m_Models;
};