
#include <Windows.h>
#include "AssimpApp.h"

#ifdef _CONSOLE
int main(int argc, char* argv[])
#else
int WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow
	)
#endif
{
	AssimpApp app("title", 1280, 720);
	app.Run();
	return 0;
}