#pragma once
#include "Camera.h"
#include <GLFW\glfw3.h>

class FlyCamera : public Camera
{
public:
	FlyCamera();
	FlyCamera(GLFWwindow* a_window, float a_moveSpeed, float a_mouseSensitivity);
	~FlyCamera();

	void Update(float a_deltaTime);
	/*Sets the movement keys.*/
	void SetInputKeys(int a_forwardKey, int a_backwardKey, int a_leftKey, int a_rightKey, int a_controlEnableKey);
	/*Sets the look sensitivity.*/
	void SetMouseSensitivity(float a_mouseSensitivity);
	/*Sets the movement speed.*/
	void SetMoveSpeed(float a_moveSpeed);

protected:
	int m_ForwardKey;
	int m_BackwardKey;
	int m_LeftKey;
	int m_RightKey;
	int m_ControlEnableKey;
	bool m_ControlEnabled;

	float m_MoveSpeed, m_MouseSensitivity;
	GLFWwindow* m_Window;
};