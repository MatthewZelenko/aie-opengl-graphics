#pragma once
#include "BoundObject.h"

class BoundingSphere : public BoundObject
{
public:
	BoundingSphere();
	~BoundingSphere();

	void Reset();
	void Fit(std::vector<FBXVertex>& a_points);

	void SetCenter(glm::vec3 a_value);
	void SetRadius(float a_value);
	void ScaleRadius(float a_scale);

	glm::vec3 GetCenter() { return m_Center; }
	float GetRadius() { return m_Radius; }

private:
	glm::vec3 m_Min, m_Max;
	glm::vec3 m_Center;
	float m_Radius;
};