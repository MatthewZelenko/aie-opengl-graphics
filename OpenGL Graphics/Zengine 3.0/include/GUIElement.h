#pragma once
#include <GLM\glm.hpp>

enum Controls
{
	BUTTON,
	SLIDER,
	IMAGE
};

class GUIElement
{
public:
	GUIElement(Controls type);
	virtual ~GUIElement();

	virtual void ProcessInput();

	Controls GetType() { return m_Type; }

	void SetPosition(glm::vec2 position);
	void SetSize(glm::vec2 size);

	glm::vec2 GetPosition() { return m_Position; }
	glm::vec2 GetSize() { return m_Size; }

protected:
	glm::vec2 m_Position;
	glm::vec2 m_Size;

private:
	Controls m_Type;
};

