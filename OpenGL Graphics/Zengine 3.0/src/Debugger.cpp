#include "Debugger.h"
#include "gl_core_4_4.h"
#include <GLM\gtc\matrix_transform.hpp>
#include <GLM\gtc\constants.hpp>

Debugger::Debugger(): m_GridVAO(0), m_SphereVAO(0), m_SphereIndexSize(0)
{
}
Debugger::~Debugger()
{
	if (m_GridVAO)
		glDeleteVertexArrays(1, &m_GridVAO);
	if (m_SphereVAO)
		glDeleteVertexArrays(1, &m_SphereVAO);
}
void Debugger::ClearObjects()
{
	m_Spheres.clear();
}

void Debugger::Init(glm::mat4& a_projection)
{
	m_Shader.CreateFromFile( "../Zengine 3.0/resources/shaders/Debugger.vert", nullptr, "../Zengine 3.0/resources/shaders/Debugger.frag");
	m_Shader.Use();
	m_Shader.SendMat4("projection", 1, a_projection);
	CreateSphere();
}
void Debugger::CreateGrid(unsigned int a_rows, unsigned int a_cols, glm::vec4& a_colour)
{
	if (!a_rows && !a_cols)
		return;

	if (m_GridVAO)
		glDeleteVertexArrays(1, &m_GridVAO);

	m_GridColour = a_colour;

	m_GridRows = a_rows;
	m_GridCols = a_cols;

	int avg = (a_rows + a_cols) / 2;
	int x = (int)a_rows;
	int y = (int)a_cols;

	Vertex* vertices = new Vertex[(a_rows + a_cols + 1) * 4];
	unsigned int counter = 0;
	glm::vec4 white(1.0f);
	glm::vec4 black(0.0f, 0.0f, 0.0f, 1.0f);
	for (int i = 0; i < (a_rows + a_cols + 1); ++i)
	{
		vertices[counter].Position = glm::vec4(-x + i, 0, y, 1);
		vertices[counter].Colour = (i == avg ? black : white);
		++counter;

		vertices[counter].Position = glm::vec4(-x + i, 0, -y, 1);
		vertices[counter].Colour = (i == avg ? black : white);
		++counter;

		vertices[counter].Position = glm::vec4(x, 0, -y + i, 1);
		vertices[counter].Colour = (i == avg ? black : white);
		++counter;

		vertices[counter].Position = glm::vec4(-x, 0, -y + i, 1);
		vertices[counter].Colour = (i == avg ? black : white);
		++counter;
	}

	unsigned int VBO;

	glGenVertexArrays(1, &m_GridVAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(m_GridVAO);
	//VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, ((a_rows + a_cols + 1) * 4) * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)(sizeof(glm::vec4)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	delete[] vertices;
}
void Debugger::CreateSphere()
{
	unsigned int rings = 20;
	unsigned int sectors = 20;
	float const R = 1.0f / (float)(rings - 1);
	float const S = 1.0f / (float)(sectors - 1);
	int r, s;

	std::vector<Vertex> sphereVertices;
	sphereVertices.resize(rings * sectors);
	std::vector<Vertex>::iterator ver = sphereVertices.begin();
	std::vector<unsigned int> sphereIndices;
	m_SphereIndexSize = rings * sectors * 6;
	sphereIndices.resize(m_SphereIndexSize);
	std::vector<unsigned int>::iterator ind = sphereIndices.begin();
	for (r = 0; r < rings; ++r)
	{
		for (s = 0; s < sectors; ++s)
		{
			float const y = sin(-glm::half_pi<float>() + glm::pi<float>() * r * R);
			float const x = cos(glm::two_pi<float>() * s * S) * sin(glm::pi<float>() * r * R);
			float const z = sin(glm::two_pi<float>() * s * S) * sin(glm::pi<float>() * r * R);

			Vertex vert;
			vert.Position = glm::vec4(x, y, z, 1.0f);
			vert.Colour = glm::vec4(1.0f);

			*ver++ = vert;
		}
	}
	for (r = 0; r < rings - 1; ++r)
	{
		for (s = 0; s < sectors - 1; ++s)
		{
			*ind++ = r * sectors + s;
			*ind++ = (r + 1) * sectors + s;
			*ind++ = (r + 1) * sectors + (s + 1);
			*ind++ = (r + 1) * sectors + (s + 1);
			*ind++ = r * sectors + (s + 1);
			*ind++ = r * sectors + s;
		}
	}

	if (m_SphereVAO)
		glDeleteVertexArrays(1, &m_SphereVAO);

	unsigned int VBO, EBO;
	glGenVertexArrays(1, &m_SphereVAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(m_SphereVAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sphereVertices.size() * sizeof(Vertex), sphereVertices.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sphereIndices.size() * sizeof(unsigned int), sphereIndices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, Position)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, Colour)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Debugger::AddSphere(glm::vec3& a_center, float a_radius, glm::vec4& a_colour)
{
	m_Spheres.push_back({ a_center, a_radius, a_colour });
}

void Debugger::Draw(glm::mat4& a_view)
{
	if (m_Shader.GetID())
	{
		m_Shader.Use();
		m_Shader.SendMat4("view", 1, a_view);
		DrawGrid();
		DrawSpheres();
	}
}
void Debugger::DrawGrid()
{
	if (m_GridVAO)
	{
		m_Shader.SendVec4("colour", 1, m_GridColour);
		m_Shader.SendMat4("model", 1, glm::mat4());
		glBindVertexArray(m_GridVAO);
		glDrawArrays(GL_LINES, 0, (m_GridRows + m_GridCols + 1) * 4);
		glBindVertexArray(0);
	}
}
void Debugger::DrawSpheres()
{
	if (m_SphereVAO)
	{
		for (unsigned int i = 0; i < m_Spheres.size(); ++i)
		{
			m_Shader.SendVec4("colour", 1, m_Spheres[i].Colour);
			glm::mat4 model;
			model = glm::translate(model, m_Spheres[i].Center);
			model = glm::scale(model, glm::vec3(m_Spheres[i].Radius));
			m_Shader.SendMat4("model", 1, model);
			glBindVertexArray(m_SphereVAO);
			glDrawElements(GL_TRIANGLES, m_SphereIndexSize, GL_UNSIGNED_INT, 0);
		}
	}
}