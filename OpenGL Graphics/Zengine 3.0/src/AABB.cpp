#include "AABB.h"

AABB::AABB()
{
	m_Type = BOX;
	Reset();
}
AABB::~AABB()
{
}

void AABB::Reset()
{
	m_Min.x = m_Min.y = m_Min.z = 1e37f;
	m_Max.x = m_Max.y = m_Max.z = -1e37f;
}
void AABB::Fit(std::vector<FBXVertex>& a_points)
{
	for (auto& p: a_points)
	{
		//min
		if (p.position.x < m_Min.x) m_Min.x = p.position.x;
		if (p.position.y < m_Min.y) m_Min.y = p.position.y;
		if (p.position.z < m_Min.z) m_Min.z = p.position.z;
		//max
		if (p.position.x > m_Max.x) m_Max.x = p.position.x;
		if (p.position.y > m_Max.y) m_Max.y = p.position.y;
		if (p.position.z > m_Max.z) m_Max.z = p.position.z;
	}
}

void AABB::SetMin(glm::vec3 a_value)
{
	m_Min = a_value;
}
void AABB::SetMax(glm::vec3 a_value)
{
	m_Max = a_value;
}

void AABB::SetPosition(glm::vec3 a_position)
{
	glm::vec3 size = m_Max - m_Min;
	m_Min = a_position;
	m_Max = m_Min + size;
}
void AABB::AddPosition(glm::vec3 a_position)
{
	m_Min += a_position;
	m_Max += a_position;
}