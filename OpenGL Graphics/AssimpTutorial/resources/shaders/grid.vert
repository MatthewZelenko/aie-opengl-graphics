#version 330
layout(location=0)in vec4 Position;
layout(location=1)in vec4 Colour;

uniform mat4 Projection;
uniform mat4 View;

out vec4 vColour;

void main()
{
	vColour = Colour;
	gl_Position = Projection * View * Position;
}