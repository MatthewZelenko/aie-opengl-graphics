#version 330 core

in vec2 vTextureCoords;

uniform sampler2D Texture;

out vec4 fColour;

void main()
{
	fColour = texture(Texture, vTextureCoords);
}