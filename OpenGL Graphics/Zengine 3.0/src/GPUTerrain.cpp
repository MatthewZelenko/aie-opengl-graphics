#include "GPUTerrain.h"
#include "gl_core_4_4.h"
#include "Perlin.h"
#include <GLM\gtc\matrix_transform.hpp>

GPUTerrain::GPUTerrain() : m_uiTActiveBuffer(0), m_uiWActiveBuffer(0), m_bTIsDirty(true), m_bWIsDirty(true), m_iWaterSeed(0),
m_fMaxTreeSize(1.0f), m_fMinTreeSize(1.0f), m_fMinTreeYPosition(0.0f), m_fMaxTreeYPosition(1.0f), m_fMaxTreeSteep(0.9f), m_fTreeSpawnChance(10), m_WaterTexture(nullptr)
{
	m_TVAO[0] = 0;
	m_TVAO[1] = 0;
	m_TVBO[0] = 0;
	m_TVBO[1] = 0;
	m_WVAO[0] = 0;
	m_WVAO[1] = 0;
	m_WVBO[0] = 0;
	m_WVBO[1] = 0;
}
GPUTerrain::~GPUTerrain()
{
}
void GPUTerrain::Destroy()
{
	for (auto iter : m_Trees)
	{
		iter.Destroy();
	}
	m_Trees.clear();
	m_UpdateShader.Destroy();
	if (m_TVAO[0])
		glDeleteVertexArrays(1, &m_TVAO[0]);
	if (m_TVAO[1])
		glDeleteVertexArrays(1, &m_TVAO[1]);
	if (m_TVBO[0])
		glDeleteBuffers(1, &m_TVBO[0]);
	if (m_TVBO[1])
		glDeleteBuffers(1, &m_TVBO[1]);
	if (m_WVAO[0])
		glDeleteVertexArrays(1, &m_WVAO[0]);
	if (m_WVAO[1])
		glDeleteVertexArrays(1, &m_WVAO[1]);
	if (m_WVBO[0])
		glDeleteBuffers(1, &m_WVBO[0]);
	if (m_WVBO[1])
		glDeleteBuffers(1, &m_WVBO[1]);
}

//Terrain
void GPUTerrain::Init(int a_rows, int a_cols, float a_width, float a_height, int a_seed, float a_amplitude, float a_frequency, float a_persistence, int a_octaves)
{
	m_Vertices.clear();
	Destroy();
	const char* varyings[] = { "gPosition", "gNormal", "gTexCoord" };
	m_UpdateShader.CreateFromFile("../Zengine 3.0/resources/shaders/TerrainUpdate.vert", "../Zengine 3.0/resources/shaders/TerrainUpdate.geom", nullptr, varyings, 3);
	m_iRows = a_rows;
	m_fWidth = a_width;
	m_fHeight = a_height;
	m_iCols = a_cols;
	m_iSeed = a_seed;
	m_UpdateShader.Use();
	m_iOctaves = a_octaves;
	m_UpdateShader.SendInt("Octaves", a_octaves);
	m_fAmplitude = a_amplitude;
	m_UpdateShader.SendFloat("Amplitude", a_amplitude);
	m_fFrequency = a_frequency;
	m_UpdateShader.SendFloat("Frequency", a_frequency);
	m_fPersistence = a_persistence;
	m_UpdateShader.SendFloat("Persistence", a_persistence);
	GenerateGrid();
	CreateBuffers();
	GenerateTerrain(a_seed);
}
void GPUTerrain::CreateBuffers()
{
	//Terrain
	glGenVertexArrays(2, m_TVAO);
	glGenBuffers(2, m_TVBO);
	/////Buffer 1/////
	glBindVertexArray(m_TVAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_TVBO[0]);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(GPUTerrainVertex), &m_Vertices[0], GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, TexCoord));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/////Buffer 2/////
	glBindVertexArray(m_TVAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_TVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(GPUTerrainVertex), &m_Vertices[0], GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, TexCoord));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Water
	glGenVertexArrays(2, m_WVAO);
	glGenBuffers(2, m_WVBO);
	/////Buffer 1/////
	glBindVertexArray(m_WVAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_WVBO[0]);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(GPUTerrainVertex), &m_Vertices[0], GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, TexCoord));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/////Buffer 2/////
	glBindVertexArray(m_WVAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_WVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(GPUTerrainVertex), &m_Vertices[0], GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GPUTerrainVertex), (void*)offsetof(GPUTerrainVertex, TexCoord));
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void GPUTerrain::GenerateGrid()
{
	float texCoordX = 1.0f / (m_iRows - 1.0f);
	float texCoordY = 1.0f / (m_iCols - 1.0f);
	float halfWidth = m_fWidth / 2.0f;
	float halfHeight = m_fHeight / 2.0f;
	int halfRow = m_iRows / 2;
	int halfCol = m_iCols / 2;

	m_Vertices.reserve(m_iRows * m_iCols);
	for (int z = 0; z < m_iCols; ++z)
	{
		for (int x = 0; x < m_iRows; ++x)
		{
			GPUTerrainVertex vertex;
			//top left
			vertex.Position.x = (float)(x - halfRow) * m_fWidth - halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_fHeight - halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x;
			vertex.TexCoord.y = (float)z;
			m_Vertices.push_back(vertex);
			//top right
			vertex.Position.x = (float)(x - halfRow) * m_fWidth + halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_fHeight - halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x + 1.0f;
			vertex.TexCoord.y = (float)z;
			m_Vertices.push_back(vertex);
			//bottom right
			vertex.Position.x = (float)(x - halfRow) * m_fWidth + halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_fHeight + halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x + 1.0f;
			vertex.TexCoord.y = (float)z + 1.0f;
			m_Vertices.push_back(vertex);
			//bottom right
			vertex.Position.x = (float)(x - halfRow) * m_fWidth + halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_fHeight + halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x + 1.0f;
			vertex.TexCoord.y = (float)z + 1.0f;
			m_Vertices.push_back(vertex);
			//bottom left
			vertex.Position.x = (float)(x - halfRow) * m_fWidth - halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_fHeight + halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.x = (float)x;
			vertex.TexCoord.y = (float)z + 1.0f;
			m_Vertices.push_back(vertex);
			//top left
			vertex.Position.x = (float)(x - halfRow) * m_fWidth - halfWidth;
			vertex.Position.z = (float)(z - halfCol) * m_fHeight - halfHeight;
			vertex.Position.y = 0;
			vertex.TexCoord.y = (float)z;
			m_Vertices.push_back(vertex);
		}
	}
}
void GPUTerrain::GenerateTerrain(int a_seed)
{
	if (m_bTIsDirty)
	{
		m_iSeed = a_seed;
		m_UpdateShader.Use();
		m_UpdateShader.SendInt("Seed", m_iSeed);
		m_UpdateShader.SendInt("Octaves", m_iOctaves);
		m_UpdateShader.SendFloat("Amplitude", m_fAmplitude);
		m_UpdateShader.SendFloat("Frequency", m_fFrequency);
		m_UpdateShader.SendFloat("Persistence", m_fPersistence);

		glEnable(GL_RASTERIZER_DISCARD);
		glBindVertexArray(m_TVAO[m_uiTActiveBuffer]);
		unsigned int otherBuffer = (m_uiTActiveBuffer + 1) % 2;

		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_TVBO[otherBuffer]);
		glBeginTransformFeedback(GL_TRIANGLES);
		glDrawArrays(GL_TRIANGLES, 0, m_Vertices.size());
		glEndTransformFeedback();
		glDisable(GL_RASTERIZER_DISCARD);
		glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_Vertices.size() * sizeof(GPUTerrainVertex), m_Vertices.data());
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
		glBindVertexArray(0);
		m_uiTActiveBuffer = otherBuffer;
		m_bTIsDirty = false;
	}
}
void GPUTerrain::AddTextures(Texture* texture)
{
	m_Textures.push_back(texture);
}
void GPUTerrain::SetAmplitude(float a_amplitude)
{
	m_fAmplitude = a_amplitude;
	m_bTIsDirty = true;
}
void GPUTerrain::SetPersistence(float a_persistence)
{
	m_fPersistence = a_persistence;
	m_bTIsDirty = true;
}
void GPUTerrain::SetFrequency(float a_frequency)
{
	m_fFrequency = a_frequency;
	m_bTIsDirty = true;
}
void GPUTerrain::DrawTerrain(Shader a_shader, Camera* a_camera)
{
	if (m_TVAO)
	{
		glBindVertexArray(m_TVAO[m_uiTActiveBuffer]);
		if (m_Textures.size())
		{
			glActiveTexture(GL_TEXTURE0);
			m_Textures[0]->Bind();
			if (m_Textures.size() > 1)
			{
				glActiveTexture(GL_TEXTURE1);
				m_Textures[1]->Bind();
				if (m_Textures.size() > 2)
				{
					glActiveTexture(GL_TEXTURE2);
					m_Textures[2]->Bind();
					if (m_Textures.size() > 3)
					{
						glActiveTexture(GL_TEXTURE3);
						m_Textures[3]->Bind();
					}
				}
			}
		}
		glDrawArrays(GL_TRIANGLES, 0, m_Vertices.size());
	}
	glActiveTexture(GL_TEXTURE0);
}
///////////////////////////////
//Trees
void GPUTerrain::GenerateTrees()
{
	for (auto iter : m_Trees)
	{
		iter.Destroy();
	}
	m_Trees.clear();

	Model* model;
	model = m_TreeModels[0];
	BoundingSphere treeBound;
	for (int i = 0; i < model->GetFBX()->getMeshCount(); ++i)
	{
		std::vector<FBXVertex> modelVerts = model->GetFBX()->getMeshByIndex(i)->m_vertices;
		treeBound.Fit(modelVerts);
	}

	for (int i = 0; i < m_Vertices.size() / 6; ++i)
	{
		int rng = rand() % 100 + 1;
		if (rng <= m_fTreeSpawnChance)
		{
			int x = (i) % m_iRows;
			int z = (i) / m_iRows;

			glm::vec3 vertPosition = GetPositionFromIndex(x, z);
			glm::vec3 n = GetNormalFromIndex(x, z);
			float dot = glm::dot(n, glm::vec3(0, 1, 0));

			if (vertPosition.y > m_fMinTreeYPosition && vertPosition.y < m_fMaxTreeYPosition && dot > m_fMaxTreeSteep)
			{
				SceneObject tree;
				
				tree.SetModel(model);
				tree.SetPosition(vertPosition);
				//size
				int sizeRng = rand() % 100;
				float size = ((m_fMaxTreeSize - m_fMinTreeSize) * (sizeRng / 100.0f)) + m_fMinTreeSize;
				tree.SetSize(glm::vec3(size));
				//rotation
				tree.SetRotation(n);
				//bounds
				BoundingSphere* newBound = new BoundingSphere();
				*newBound = treeBound;
				//newBound->ScaleRadius(size / 2.0f);
				newBound->SetCenter(tree.GetPosition());
				tree.SetBound(newBound);

				m_Trees.push_back(tree);
			}
		}
	}


}
void GPUTerrain::AddTreeModel(Model* a_model)
{
	m_TreeModels.push_back(a_model);
}
void GPUTerrain::SetMinAndMaxTreeSize(float a_min, float a_max)
{
	m_fMinTreeSize = a_min;
	m_fMaxTreeSize = a_max;
}
void GPUTerrain::SetMinAndMaxTreeYPosition(float a_min, float a_max)
{
	m_fMinTreeYPosition = a_min;
	m_fMaxTreeYPosition = a_max;
}
void GPUTerrain::SetMaxTreeSteep(float a_max)
{
	m_fMaxTreeSteep = a_max;
}
void GPUTerrain::SetTreeSpawnChance(int value)
{
	m_fTreeSpawnChance = value;
}
void GPUTerrain::DrawTrees(Shader a_shader, Camera* a_camera)
{
	for (int i = 0; i < m_Trees.size(); ++i)
	{
		if (a_camera->InsideFrustum(m_Trees[i].GetBounds()))
		{
			m_Trees[i].Draw(a_shader);
		}
	}
}
///////////////////////////////
//Indexing
float GPUTerrain::GetHeightFromPosition(int a_x, int a_z)
{
	int index1 = (int)((((int)(a_z + m_iCols / 2 * m_fHeight) / (int)m_fHeight) * m_iRows + ((int)(a_x + m_iRows / 2 * m_fWidth) / (int)m_fWidth)) * 6);
	int index2 = index1 + 2;
	if (index1 >= 0 && index1 < (m_iRows)* (m_iCols)* 6 && index2 >= 0 && index2 < (m_iRows)* (m_iCols)* 6)
	{
		float y = (m_Vertices[index1].Position.y + m_Vertices[index2].Position.y) / 2.0f;
		return y;
	}
	else return 0;
}
glm::vec3 GPUTerrain::GetNormalFromPosition(int a_x, int a_z)
{
	int index1 = (int)((((int)(a_z + m_iCols / 2 * m_fHeight) / (int)m_fHeight) * m_iRows + ((int)(a_x + m_iRows / 2 * m_fWidth) / (int)m_fWidth)) * 6);
	if (index1 >= 0 && index1 < (m_iRows)* (m_iCols)* 6)
	{
		glm::vec3 n = m_Vertices[index1].Normal;
		return n;
	}
	else return glm::vec3(0);
}
glm::vec3 GPUTerrain::GetPositionFromPosition(int a_x, int a_z)
{
	int index1 = (int)((((int)(a_z + m_iCols / 2 * m_fHeight) / (int)m_fHeight) * m_iRows + ((int)(a_x + m_iRows / 2 * m_fWidth) / (int)m_fWidth)) * 6);
	int index2 = index1 + 2;
	if (index1 >= 0 && index1 < (m_iRows)* (m_iCols)* 6 && index2 >= 0 && index2 < (m_iRows)* (m_iCols)* 6)
	{
		glm::vec3 n = (m_Vertices[index1].Position + m_Vertices[index2].Position) / 2.0f;
		return n;
	}
	else return glm::vec3(0);
}

float GPUTerrain::GetHeightFromIndex(int a_x, int a_z)
{
	int index1 = (a_z * m_iRows + a_x) * 6;
	int index2 = index1 + 2;
	if (index1 >= 0 && index1 < (m_iRows) * (m_iCols) * 6 && index2 >= 0 && index2 < (m_iRows) * (m_iCols) * 6)
	{
		float y = (m_Vertices[index1].Position.y + m_Vertices[index2].Position.y) / 2.0f;
		return y;
	}
	else return 0;
}
glm::vec3 GPUTerrain::GetNormalFromIndex(int a_x, int a_z)
{
	int index1 = (a_z * m_iRows + a_x) * 6;
	if (index1 >= 0 && index1 < (m_iRows)* (m_iCols)* 6)
	{
		glm::vec3 n = m_Vertices[index1].Normal;
		return n;
	}
	else return glm::vec3(0);
}
glm::vec3 GPUTerrain::GetPositionFromIndex(int a_x, int a_z)
{
	int index1 = (a_z * m_iRows + a_x) * 6;
	int index2 = index1 + 2;
	if (index1 >= 0 && index1 < (m_iRows)* (m_iCols)* 6 && index2 >= 0 && index2 < (m_iRows)* (m_iCols)* 6)
	{
		glm::vec3 n = (m_Vertices[index1].Position + m_Vertices[index2].Position) / 2.0f;
		return n;
	}
	else return glm::vec3(0);
}
///////////////////////////////

//water

void GPUTerrain::GenerateWater()
{
	m_UpdateShader.Use();
	m_iWaterSeed += 1.0f;
	m_UpdateShader.SendInt("Seed", (int)m_iWaterSeed);
	m_UpdateShader.SendInt("Octaves", 1);
	m_UpdateShader.SendFloat("Amplitude", 20);
	m_UpdateShader.SendFloat("Frequency", 0.02f);
	m_UpdateShader.SendFloat("Persistence", 0.005f);

		glEnable(GL_RASTERIZER_DISCARD);
		glBindVertexArray(m_WVAO[m_uiWActiveBuffer]);
		unsigned int otherBuffer = (m_uiWActiveBuffer + 1) % 2;

		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_WVBO[otherBuffer]);
		glBeginTransformFeedback(GL_TRIANGLES);
		glDrawArrays(GL_TRIANGLES, 0, m_Vertices.size());
		glEndTransformFeedback();
		glDisable(GL_RASTERIZER_DISCARD);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
		glBindVertexArray(0);
		m_uiWActiveBuffer = otherBuffer;
}
void GPUTerrain::SetWaterTexture(Texture* a_water)
{
	m_WaterTexture = a_water;
}
void GPUTerrain::SetWaterPosition(glm::vec3 position)
{
	m_WPosition = position;
}
void GPUTerrain::SetWaterYPosition(float y)
{
	m_WPosition.y = y;
}
void GPUTerrain::DrawWater(Shader a_shader, Camera* a_camera)
{
	if (m_WVAO)
	{
		glBindVertexArray(m_WVAO[m_uiWActiveBuffer]);
		if (m_WaterTexture->GetID())
		{
			glm::mat4 mat;
			mat = glm::translate(mat, m_WPosition);
			a_shader.SendMat4("Model", 1, mat);
			a_shader.SendVec4("Colour", 1, glm::vec4(1.0f, 1.0f, 1.0f, 0.5f));
			glActiveTexture(GL_TEXTURE0);
			m_WaterTexture->Bind();
		}
		glDrawArrays(GL_TRIANGLES, 0, m_Vertices.size());
	}
	glActiveTexture(GL_TEXTURE0);
}
