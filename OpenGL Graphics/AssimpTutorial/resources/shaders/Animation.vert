#version 330 core
layout(location=0)in vec3 Position;
layout(location=1)in vec3 Normal;
layout(location=2)in vec2 TexCoord;
layout(location=3)in vec4 Tangent;
layout(location=4)in vec4 Weights;
layout(location=5)in vec4 Indices;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;

out vec2 vTexCoord;
out vec3 vNormal;
out vec3 vPosition;

const int MAX_BONES = 128;
uniform mat4 Bones[MAX_BONES];

void main()
{
	vNormal = mat3(transpose(inverse(Model))) * Normal;
	vTexCoord = TexCoord;

	ivec4 index = ivec4(Indices);

	vec4 position = vec4(Position, 1);
	vec4 outPosition;
	outPosition = Bones[index.x] * position * Weights.x;
	outPosition += Bones[index.y] * position * Weights.y;
	outPosition += Bones[index.z] * position * Weights.z;
	outPosition += Bones[index.w] * position * Weights.w;
	vPosition = vec3(outPosition);

	gl_Position = Projection * View * Model * outPosition;
}