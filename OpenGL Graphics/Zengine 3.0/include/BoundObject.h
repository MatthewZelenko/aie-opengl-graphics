#pragma once
#include <GLM\glm.hpp>
#include <vector>
#include "Model.h"

enum BoundType
{
	BOX,
	SPHERE
};
class BoundObject
{
public:
	BoundObject();
	virtual ~BoundObject();

	virtual void Fit(std::vector<FBXVertex>& a_points) = 0;

	BoundType GetType() { return m_Type; }

protected:
	BoundType m_Type;
};