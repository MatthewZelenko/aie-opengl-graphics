#version 420 core

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;
layout(location=2) in vec2 TexCoord;

uniform mat4 Projection;
uniform mat4 View;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoord;

void main()
{
	vTexCoord = TexCoord;
	vPosition = Position;
	vNormal = Normal; //mat3(transpose(inverse(model))) * 
	gl_Position = Projection * View * vec4(vPosition, 1);
}