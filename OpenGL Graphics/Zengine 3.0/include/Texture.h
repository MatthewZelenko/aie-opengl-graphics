#pragma once
#include <string>

class Texture
{
public:
	Texture();
	Texture(const Texture& a_other);
	~Texture();
	void Destroy();

	Texture& operator=(const Texture& a_other);

	Texture CreateFromFile(const std::string a_file);
	Texture Bind();
	void UnBind();

	void SetWrapS(int a_param);
	void SetWrapT(int a_param);
	void SetMinFilter(int a_param);
	void SetMagFilter(int a_param);

	unsigned int GetID() { return m_ID; }
	int GetHeight() { return m_iHeight; }
	int GetWidth() { return m_iWidth; }
	std::string GetName() { return m_strName; }

protected:
	std::string m_strName;
	unsigned int m_ID;
	int m_iWidth;
	int m_iHeight;
	int m_iImageFormat;

	int m_iWrapTParam, m_iWrapSParam, m_iMinFilterParam, m_iMagFilterParam;
};