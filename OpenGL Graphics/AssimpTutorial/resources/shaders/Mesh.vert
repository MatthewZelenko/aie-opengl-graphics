#version 330 core

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;
layout(location=2) in vec2 TexCoord;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoord;

void main()
{
	vPosition = Position;
	vNormal = mat3(transpose(inverse(Model))) * Normal;
	vTexCoord = TexCoord;
	gl_Position = Projection * View * Model * vec4(Position, 1);
}