#version 330 core

layout(location=0)in vec3 Position;
layout(location=1)in vec3 Normal;
layout(location=2)in vec2 TexCoord;

uniform int Seed = 0;
uniform float Amplitude = 5.0f;
uniform float Frequency = 0.5f;
uniform float Persistence = 0.2f;
uniform int Octaves = 4;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCoord;

const float PI = 3.141592653589793238462643383;

float CosLerp(float a_a, float a_b, float a_x)
{
	float ft = a_x * PI;
	float f = (1 - cos(ft)) * 0.5f;
	return a_a * (1 - f) + a_b * f;
}
float GenNoise(int a_x, int a_y)
{
	int n = a_x + a_y * 57;
	n = (n << 13) ^ n;
	return (1.0f - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}
float GenSmoothNoise(float a_x, float a_y)
{
	float corners = (GenNoise(int(a_x - 1), int(a_y - 1)) + GenNoise(int(a_x + 1), int(a_y - 1)) + GenNoise(int(a_x - 1), int(a_y + 1)) + GenNoise(int(a_x + 1), int(a_y + 1))) / 8;
	float sides = (GenNoise(int(a_x - 1), int(a_y)) + GenNoise(int(a_x + 1), int(a_y)) + GenNoise(int(a_x), int(a_y - 1)) + GenNoise(int(a_x), int(a_y + 1))) / 8;
	float center = GenNoise(int(a_x), int(a_y)) / 5;
	return corners + sides + center;
}
float LerpNoise(float a_x, float a_y)
{
	int ix = int(a_x);
	float fx = a_x - ix;

	int iy = int(a_y);
	float fy = a_y - iy;

	float v1 = GenSmoothNoise(ix, iy);
	float v2 = GenSmoothNoise(ix + 1, iy);
	float v3 = GenSmoothNoise(ix, iy + 1);
	float v4 = GenSmoothNoise(ix + 1, iy + 1);

	float i1 = CosLerp(v1, v2, fx);
	float i2 = CosLerp(v3, v4, fx);
	return CosLerp(i1, i2, fy);
}

void main()
{
	float total = 0;
	float p = Persistence;
	int n = Octaves;
	for (int i = 0; i < n; ++i)
	{
		float frequency = pow(2, i) * Frequency;
		float amplitude = pow(p, i) * Amplitude;
		total += LerpNoise(abs((Position.x + Seed) * frequency), abs((Position.z + Seed) * frequency)) * amplitude;
	}
	vPosition = Position;
	vPosition.y = total;
	//vPosition.y = float(LerpNoise(int(abs((vPosition.x + Seed)* Frequency)), int(abs((vPosition.z + Seed) * Frequency))) * Amplitude);

	vNormal = Normal;
	vTexCoord = TexCoord;
}