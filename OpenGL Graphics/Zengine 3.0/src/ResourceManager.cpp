#include "ResourceManager.h"

ResourceManager::ResourceManager()
{
}
ResourceManager::~ResourceManager()
{
}

void ResourceManager::Destroy()
{
	std::map<std::string, Texture>::iterator textureIter;
	for (textureIter = m_Textures.begin(); textureIter != m_Textures.end(); ++textureIter)
	{
		textureIter->second.Destroy();
	}
	std::map<std::string, Shader>::iterator shaderIter;
	for (shaderIter = m_Shaders.begin(); shaderIter != m_Shaders.end(); ++shaderIter)
	{
		shaderIter->second.Destroy();
	}
	std::map<std::string, Model>::iterator modelIter;
	for (modelIter = m_Models.begin(); modelIter != m_Models.end(); ++modelIter)
	{
		modelIter->second.Destroy();
	}
}

Texture* ResourceManager::LoadTexture(char* a_filePath)
{
	if (m_Textures.find(a_filePath) != m_Textures.end())
	{
		m_Textures[a_filePath].Destroy();
		m_Textures.erase(a_filePath);
	}
	Texture texture;
	texture.CreateFromFile(a_filePath);
	m_Textures[a_filePath] = texture;
	return &m_Textures[a_filePath];
}
Shader* ResourceManager::LoadShader(char* a_shaderName, char* a_vertFilePath, char* a_geomFilePath, char* a_fragFilePath, const char** a_varyings, const int a_varyingsSize)
{
	if (m_Shaders.find(a_shaderName) != m_Shaders.end())
	{
		m_Shaders[a_shaderName].Destroy();
		m_Shaders.erase(a_shaderName);
	}
	Shader shader;
	shader.CreateFromFile(a_vertFilePath, a_geomFilePath, a_fragFilePath, a_varyings, a_varyingsSize);
	m_Shaders[a_shaderName] = shader;
	return &m_Shaders[a_shaderName];
}
Model* ResourceManager::LoadModel(char* a_filePath)
{
	if (m_Models.find(a_filePath) != m_Models.end())
	{
		m_Models[a_filePath].Destroy();
		m_Models.erase(a_filePath);
	}
	Model model;
	model.LoadModel(a_filePath);
	m_Models[a_filePath] = model;
	return &m_Models[a_filePath];
}