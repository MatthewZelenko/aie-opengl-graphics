#pragma once
#include <GLM\glm.hpp>
#include <vector>
#include "Shader.h"
#include "Texture.h"
#include "Model.h"
#include "SceneObject.h"
#include "Camera.h"

struct GPUTerrainVertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoord;
};
class GPUTerrain
{
public:
	GPUTerrain();
	~GPUTerrain();

	void Destroy();

	void Init(int a_rows, int a_cols, float a_width, float a_height, int a_seed, float a_amplitude = 1.0f, float a_frequency = 1.0f, float a_persistence = 0.5f, int a_octaves = 4);
	void GenerateTerrain(int a_seed);
	void GenerateWater();
	void AddTextures(Texture* texture);
	void DrawTerrain(Shader a_shader, Camera* a_camera);
	void DrawTrees(Shader a_shader, Camera* a_camera);
	void DrawWater(Shader a_shader, Camera* a_camera);

	void SetAmplitude(float a_amplitude);
	void SetPersistence(float a_persistence);
	void SetFrequency(float a_frequency);
	void GenerateTrees();
	void AddTreeModel(Model* a_model);
	void SetMinAndMaxTreeSize(float a_min, float a_max);
	void SetMinAndMaxTreeYPosition(float a_min, float a_max);
	void SetMaxTreeSteep(float a_max);
	void SetTreeSpawnChance(int value);

	void SetWaterTexture(Texture* a_water);
	void SetWaterPosition(glm::vec3 position);
	void SetWaterYPosition(float y);

	float GetHeightFromPosition(int a_x, int a_z);
	glm::vec3 GetPositionFromPosition(int a_x, int a_z);
	glm::vec3 GetNormalFromPosition(int a_x, int a_z);

	float GetHeightFromIndex(int a_x, int a_z);
	glm::vec3 GetPositionFromIndex(int a_x, int a_z);
	glm::vec3 GetNormalFromIndex(int a_x, int a_z);

	std::vector<GPUTerrainVertex>* GetVertices() { return &m_Vertices; }
	int GetRows() { return m_iRows; }
	int GetCols() { return m_iCols; }

protected:
	void GenerateGrid();
	void CreateBuffers();

private:
	unsigned int m_TVAO[2], m_TVBO[2];
	unsigned int m_WVAO[2], m_WVBO[2];
	unsigned int m_uiTActiveBuffer, m_uiWActiveBuffer;

	Shader m_UpdateShader;

	std::vector<GPUTerrainVertex> m_Vertices;

	int m_iRows, m_iCols;
	float m_fWidth, m_fHeight;
	float m_fAmplitude, m_fFrequency, m_fPersistence;
	int m_iOctaves, m_iSeed;

	std::vector<Texture*> m_Textures;
	std::vector<Model*> m_TreeModels;
	std::vector<SceneObject> m_Trees;
	float m_fMinTreeSize, m_fMaxTreeSize;
	float m_fMinTreeYPosition, m_fMaxTreeYPosition;
	float m_fMaxTreeSteep;
	int m_fTreeSpawnChance;
	bool m_bTIsDirty, m_bWIsDirty;

	Texture* m_WaterTexture;
	float m_iWaterSeed;
	glm::vec3 m_WPosition;
};