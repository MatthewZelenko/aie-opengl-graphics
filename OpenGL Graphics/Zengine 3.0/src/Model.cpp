#include "Model.h"
#include <iostream>
#include "gl_core_4_4.h"
#include "Texture.h"

Model::Model() : m_Scene(nullptr)
{

}
Model::~Model()
{

}
void Model::Destroy()
{
	for (int i = 0; i < m_VAOs.size(); ++i)
	{
		if (m_VAOs[i])
		{
			glDeleteVertexArrays(1, &m_VAOs[i]);
		}
		if (m_VBOs[i])
		{
			glDeleteBuffers(1, &m_VBOs[i]);
		}
		if (m_EBOs[i])
		{
			glDeleteBuffers(1, &m_EBOs[i]);
		}
	}
	m_VAOs.clear();
	m_VBOs.clear();
	m_EBOs.clear();
	if (m_Scene)
	{
		delete m_Scene;
		m_Scene = nullptr;
	}
}

void Model::LoadModel(const std::string& a_path)
{
	Destroy();
	m_Scene = new FBXFile();
	bool success = m_Scene->load(a_path.c_str());
	assert(success);
	CreateBuffers();
	m_Scene->initialiseOpenGLTextures();
}
void Model::CreateBuffers()
{
	for (unsigned int i = 0; i < m_Scene->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = m_Scene->getMeshByIndex(i);

		unsigned int VAO = 0;
		unsigned int VBO = 0;
		unsigned int EBO = 0;

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

		glBufferData(GL_ARRAY_BUFFER, mesh->m_vertices.size() * sizeof(FBXVertex), mesh->m_vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->m_indices.size() * sizeof(unsigned int), mesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (char*)0 + FBXVertex::NormalOffset);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (char*)0 + FBXVertex::TexCoord1Offset);
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (char*)0 + FBXVertex::TangentOffset);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (char*)0 + FBXVertex::WeightsOffset);
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (char*)0 + FBXVertex::IndicesOffset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		m_VAOs.push_back(VAO);
		m_VBOs.push_back(VBO);
		m_EBOs.push_back(EBO);
	}
}

void Model::Draw(Shader a_shader, glm::mat4& a_model)
{
	for (unsigned int i = 0; i < m_Scene->getMeshCount(); ++i)
	{
		if (m_VAOs[i])
		{
			glBindVertexArray(m_VAOs[i]);
			if (m_Scene->getMeshByIndex(i)->m_material->textures[FBXMaterial::DiffuseTexture])
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, m_Scene->getMeshByIndex(i)->m_material->textures[FBXMaterial::DiffuseTexture]->handle);
			}
			glm::mat4 mat = a_model * m_Scene->getMeshByIndex(i)->m_globalTransform;
			a_shader.SendMat4("Model", 1, mat);

			if (i < m_Scene->getSkeletonCount())
			{
				FBXSkeleton* skeleton = m_Scene->getSkeletonByIndex(i);
				skeleton->updateBones();
				a_shader.SendMat4("Bones", skeleton->m_boneCount, *skeleton->m_bones);
			}

			glDrawElements(GL_TRIANGLES, m_Scene->getMeshByIndex(i)->m_indices.size(), GL_UNSIGNED_INT, 0);
		}
	}
}