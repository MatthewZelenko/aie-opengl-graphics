#pragma once
#include "GUIElement.h"
#include "Texture.h"

class GUISlider :
	public GUIElement
{
public:
	GUISlider();
	~GUISlider();

	void LoadFillTexture(Texture* texture);
	void LoadBackTexture(Texture* texture);

	void ProcessInput();

	//Between 0.0f - 1.0f
	void SetFillValue(float value);
	void SetBackPosition(glm::vec2 position);
	void SetFillPosition(glm::vec2 position);
	void SetBackSize(glm::vec2 size);
	void SetFillSize(glm::vec2 size);
	void SetVariablePointer(float* variable, float min, float max, bool* IsDirty = nullptr);

	Texture GetFillTexture() { return *m_FillTexture; }
	Texture GetBackTexture() { return *m_BackTexture; }
	float GetFillValue() { return m_FillValue; }
	glm::vec2 GetFillPosition() { return m_FillPosition; }
	glm::vec2 GetBackPosition() { return m_BackPosition; }
	glm::vec2 GetFillSize() { return m_FillSize; }
	glm::vec2 GetBackSize() { return m_BackSize; }

protected:
	Texture* m_FillTexture, *m_BackTexture;
	glm::vec2 m_FillPosition;
	glm::vec2 m_BackPosition;
	glm::vec2 m_FillSize;
	glm::vec2 m_BackSize;
	float* m_FillPointer;
	bool* m_IsDirty;
	float m_FillValue;
	float m_Min, m_Max;
};