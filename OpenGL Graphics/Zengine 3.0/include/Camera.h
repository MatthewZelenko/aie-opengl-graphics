#pragma once
#include <GLM\glm.hpp>
#include <GLM\gtc\quaternion.hpp>
#include "BoundObject.h"
#include "BoundingSphere.h"
#include "AABB.h"

enum PlaneType
{
	PLANE_NEAR,
	PLANE_FAR,
	PLANE_LEFT,
	PLANE_RIGHT,
	PLANE_TOP,
	PLANE_BOTTOM
};
class Camera
{
public:
	Camera();
	virtual ~Camera();

	virtual void Update(float a_deltaTime);

	/*Sets the perspective matrix.*/
	void SetPerspective(float a_fov, float a_aspect, float a_near, float a_far);
	/*Sets the position, direction and up vectors.
	Updates View Matrix.*/
	void SetView(glm::vec3& a_position, glm::vec3& a_direction, glm::vec3& a_up);

	/*Translates camera along the worlds forward axis.*/
	void TranslateWorldForward(float a_translation);
	/*Translates camera along the worlds right axis.*/
	void TranslateWorldRight(float a_translation);
	/*Translates camera along the worlds up axis.*/
	void TranslateWorldUp(float a_translation);
	/*Translates camera along the forward axis.*/
	void TranslateForward(float a_translation);
	/*Translates camera along the right axis.*/
	void TranslateRight(float a_translation);
	/*Translates camera along the up axis.*/
	void TranslateUp(float a_translation);
	/*Translates camera along the z axis.*/
	void TranslateZ(float a_translation);
	/*Translates camera along the x axis.*/
	void TranslateX(float a_translation);

	/*Rotates camera around the Worlds up axis.*/
	void RotateAroundWorldForward(float a_rotation);
	/*Rotates camera around the Worlds up axis.*/
	void RotateAroundWorldRight(float a_rotation);
	/*Rotates camera around the Worlds up axis.*/
	void RotateAroundWorldUp(float a_rotation);

	/*Rotates camera around the up axis.*/
	void RotateAroundForward(float a_rotation);
	/*Rotates camera around the up axis.*/
	void RotateAroundRight(float a_rotation);
	/*Rotates camera around the up axis.*/
	void RotateAroundUp(float a_rotation);

	/*Set the up axis.*/
	void SetUp(glm::vec3& a_up);
	/*Set the forward axis.*/
	void SetForward(glm::vec3& a_forward);
	/*Set the position*/
	void SetPosition(glm::vec3& a_position);
	/*Set the x position*/
	void SetXPosition(float a_position);
	/*Set the y position*/
	void SetYPosition(float a_position);
	/*Set the z position*/
	void SetZPosition(float a_position);



	bool InsideFrustum(BoundObject* a_boundObject);
	/*Returns view matrix.*/
	glm::mat4 GetView() { return m_View; }
	/*Returns projection matrix.*/
	glm::mat4 GetProjection() { return m_Projection; }

	/*Returns position.*/
	glm::vec3 GetPosition() { return m_Position; }
	/*Returns views forward vector.*/
	glm::vec3 GetDirection() { return glm::vec3(m_View[2]); }
	/*Returns views up vector.*/
	glm::vec3 GetUp() { return glm::vec3(m_View[1]); }
	/*Returns views right vector.*/
	glm::vec3 GetRight() { return glm::vec3(m_View[0]); }

	/*Returns a frustum plane*/
	glm::vec4 GetPlane(PlaneType a_plane) { UpdatePlanes(); return m_Planes[a_plane]; }
	/*Returns all frustum planes*/
	glm::vec4* GetPlanes() { UpdatePlanes(); return m_Planes; }


protected:
	bool SphereInsideFrustum(BoundingSphere& a_sphere);
	bool AABBInsideFrustum(AABB& a_aabb);
	void UpdateMatrices();
	void UpdatePlanes();

	glm::mat4 m_View;
	glm::mat4 m_Projection;

	glm::vec3 m_Position;
	glm::vec3 m_Forward;
	glm::vec3 m_Up;

	glm::vec4 m_Planes[6];
	bool m_DirtyPlanes;
};