#pragma once
#include <GLM\glm.hpp>
#include <vector>

class Perlin
{
public:
	static float LinearLerp(float a_a, float a_b, float a_x);
	static float CosLerp(float a_a, float a_b, float a_x);
	static float CubicLerp(float a_v0, float a_v1, float a_v2, float a_v3, float a_x);

	static float GenNoise1(int a_x);
	static float GenNoise2(int a_x, int a_y);

	static float GenSmoothNoise1(float a_x);
	static float GenSmoothNoise2(float a_x, float a_y);

	static float LerpNoise1(float a_x);
	static float LerpNoise2(float a_x, float a_y);

	static float GenPerlinNoise1(float a_x, float a_amplitude = 1.0f, float a_frequency = 1.0f, float a_persistence = 0.5f);
	static float GenPerlinNoise2(float a_x, float a_y, float a_amplitude = 1.0f, float a_frequency = 1.0f, float a_persistence = 0.5f);
private:
};