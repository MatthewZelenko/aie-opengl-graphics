#pragma once
#include "BoundObject.h"
#include "Model.h"
#include <GLM\glm.hpp>
#include <GLM\gtx\quaternion.hpp>

class SceneObject
{
public:
	SceneObject();
	virtual ~SceneObject();

	void Destroy();

	virtual void ProcessInput();
	virtual void Update();
	virtual void Draw(Shader a_shader);

	void SetModel(Model* a_model);

	Model* GetModel() { return m_Model; }

	void SetPosition(glm::vec3 a_position);
	void SetSize(glm::vec3 a_size);
	void SetRotation(float a_degrees, glm::vec3 a_axis);
	void SetRotation(glm::vec3 up);

	void AddPosition(glm::vec3 a_position);
	void AddSize(glm::vec3 a_size);

	glm::vec3 GetPosition() { return m_Position; }
	glm::vec3 GetSize() { return m_Size; }

	void SetBound(BoundObject* a_bounds);

	BoundObject* GetBounds() { return m_Bounds; }

protected:
	Model* m_Model;

	glm::vec3 m_Position;
	glm::vec3 m_Size;
	glm::quat m_Rotation;

private:
	bool m_IsDirty;
	BoundObject* m_Bounds;
};